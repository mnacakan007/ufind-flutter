import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/notifications/notifications_bloc.dart';
import 'package:ufind_flutter/blocs/offers/offers_bloc.dart';
import 'package:ufind_flutter/blocs/registration/registr_bloc.dart';
import 'package:ufind_flutter/repositories/auth_repository.dart';
import 'package:ufind_flutter/repositories/customerInfo_repository.dart';
import 'package:ufind_flutter/repositories/notification_repository.dart';
import 'package:ufind_flutter/repositories/offers_repository.dart';
import 'package:ufind_flutter/repositories/points_repository.dart';
import 'package:ufind_flutter/repositories/profile_repository.dart';
import 'package:ufind_flutter/repositories/quiz_repository.dart';
import 'package:ufind_flutter/repositories/single_offer_repository.dart';
import 'package:ufind_flutter/repositories/single_survey_repository.dart';
import 'package:ufind_flutter/repositories/surveys_repository.dart';
import 'package:ufind_flutter/screens/login_screen.dart';
import 'package:ufind_flutter/utils/class_builder.dart';


import 'blocs/auth/auth_bloc.dart';
import 'blocs/offer/single_offer_bloc.dart';
import 'blocs/points/points_bloc.dart';
import 'blocs/profile/profile_bloc.dart';
import 'blocs/quiz/quiz_bloc.dart';
import 'blocs/survey/single_survey_bloc.dart';
import 'blocs/surveys/surveys_bloc.dart';
import 'common/loading_indicator.dart';
import 'common/registration_state.dart';


class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stackTrace) {
    super.onError(bloc, error, stackTrace);
  }
}

void main() {
  ClassBuilder.registerClasses();
  BlocSupervisor.delegate = SimpleBlocDelegate();
  runApp(
      BlocProvider<AuthBloc>(
        create: (context) {
          return AuthBloc(authRepository: AuthRepository())
            ..add(AuthStarted());
        },
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {

//  final AuthRepository authRepository;
//
//  App({Key key, @required this.authRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<SurveyBloc>(
          create: (_) => SurveyBloc(
            surveyRepository: SurveyRepository(),
          ),
        ),
        BlocProvider<SingleSurveyBloc>(
          create: (_) => SingleSurveyBloc(
            singleSurveyRepository: SingleSurveyRepository(),
          ),
        ),
        BlocProvider<QuizBloc>(
          create: (_) => QuizBloc(
            quizRepository: QuizRepository(),
          ),
        ),
        BlocProvider<OfferBloc>(
          create: (_) => OfferBloc(
            offerRepository: OfferRepository(),
          ),
        ),
        BlocProvider<SingleOfferBloc>(
          create: (_) => SingleOfferBloc(
            singleOfferRepository: SingleOfferRepository(),
          ),
        ),
        BlocProvider<PointsBloc>(
          create: (_) => PointsBloc(
            pointsRepository: PointsRepository(),
          ),
        ),
        BlocProvider<NotificationBloc>(
          create: (_) => NotificationBloc(
            notificationRepository: NotificationRepository(),
          ),
        ),
        BlocProvider<RegistrationBloc>(
          create: (_) => RegistrationBloc(
            customerRepository: CustomerRepository(),
          ),
        ),
        BlocProvider<ProfileBloc>(
          create: (_) => ProfileBloc(
            profileRepository: ProfileRepository(),
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Ufind App',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: Colors.white,
          accentColor: Colors.tealAccent,
        ),
        home: BlocBuilder<AuthBloc, AuthState>(
          builder: (context, state) {
            if (state is AuthLoaded) {
              return Registration();
            }
            if (state is AuthError) {
              return LoginScreen();
            }
            if (state is AuthLoading) {
              return LoadingIndicator();
            }
            return LoadingIndicator();
          },
        ),
      ),
    );
  }
}

