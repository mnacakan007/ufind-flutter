import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

class Survey extends Equatable {
  final int createdAt;
  final int estimation;
  final String featuredImageUrl;
  final String id;
  final String partnerId;
  final dynamic result;
  final String title;
  final int totalPointsMax;
  final int totalPointsMin;
  final int updatedAt;

  const Survey({
    this.createdAt,
    this.estimation,
    this.featuredImageUrl,
    @required this.id,
    this.partnerId,
    this.result,
    this.title,
    this.totalPointsMax,
    this.totalPointsMin,
    this.updatedAt,
  });

  @override
  List<Object> get props => [
        createdAt,
        estimation,
        featuredImageUrl,
        id,
        partnerId,
        result,
        title,
        totalPointsMax,
        totalPointsMin,
        updatedAt,
      ];

  @override
  String toString() => 'SurveyResult { '
      'createdAt: $createdAt,'
      'estimation: $estimation,'
      'featuredImageUrl: $featuredImageUrl,'
      'id: $id,'
      'partnerId: $partnerId,'
      'result: $result,'
      'title: $title,'
      'totalPointsMax: $totalPointsMax,'
      'totalPointsMin: $totalPointsMin,'
      'updatedAt: $updatedAt,'
      '}';

  factory Survey.fromJson(Map<String, dynamic> json) {
    return Survey(
      createdAt: json['createdAt'] as int,
      estimation: json['estimation'] as int,
      featuredImageUrl: json['featuredImageUrl'] as String,
      id: json['id'] as String,
      partnerId: json['partnerId'] as String,
      result: json['result'] as dynamic,
      title: json['title'] as String,
      totalPointsMax: (json['totalPointsMax'] as num).toInt(),
      totalPointsMin: (json['totalPointsMin'] as num).toInt(),
      updatedAt: json['updatedAt'] as int,
    );
  }
}

class SurveysResult extends Equatable{
  final int createdAt;
  final dynamic currentField;
  final String currentFieldId;
  final String id;
  final String pointsCurrency;
  final int pointsEarned;
  final String status;
  final dynamic survey;
  final String surveyId;
  final int updatedAt;

  const SurveysResult({
    this.createdAt,
    this.currentField,
    this.currentFieldId,
    this.id,
    this.pointsCurrency,
    this.pointsEarned,
    this.status,
    this.survey,
    this.surveyId,
    this.updatedAt
  });


  @override
  List<Object> get props => [
    createdAt,
    currentField,
    currentFieldId,
    id,
    pointsCurrency,
    pointsEarned,
    status,
    survey,
    surveyId,
    updatedAt
  ];

  @override
  String toString() => 'SurveysResult { '
      'createdAt: $createdAt,'
      'currentField: $currentField,'
      'currentFieldId: $currentFieldId,'
      'id: $id,'
      'pointsCurrency: $pointsCurrency,'
      'pointsEarned: $pointsEarned,'
      'status: $status,'
      'survey: $survey,'
      'surveyId: $surveyId,'
      'updatedAt: $updatedAt,'
      '}';

  factory SurveysResult.fromJson(Map<String, dynamic> json) {
    return SurveysResult(
      createdAt: json['createdAt'] as int,
      currentField: json['currentField'] as dynamic,
      currentFieldId: json['currentFieldId'] as String,
      id: json['id'] as String,
      pointsCurrency: json['pointsCurrency'] as String,
      pointsEarned: json['pointsEarned'] as int,
      status: json['status'] as String,
      surveyId: json['surveyId'] as dynamic,
      updatedAt: json['updatedAt'] as int,
    );
  }
}