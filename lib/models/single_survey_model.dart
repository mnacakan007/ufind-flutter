import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';


class SurveyResult extends Equatable{
  final int createdAt;
  final dynamic currentField;
  final String currentFieldName;
  final String currentFieldId;
  final String id;
  final String pointsCurrency;
  final int pointsEarned;
  final String status;
  final dynamic survey;
  final String surveyId;
  final int updatedAt;
  final List<dynamic> options;

  const SurveyResult({
    this.createdAt,
    this.currentField,
    this.currentFieldName,
    this.currentFieldId,
    @required this.id,
    this.pointsCurrency,
    this.pointsEarned,
    this.status,
    this.survey,
    this.surveyId,
    this.updatedAt,
    this.options,
  });


  @override
  List<Object> get props => [
    createdAt,
    currentField,
    currentFieldName,
    currentFieldId,
    id,
    pointsCurrency,
    pointsEarned,
    status,
    survey,
    surveyId,
    updatedAt,
    options,
  ];

  @override
  String toString() => 'SurveyResult { '
      'createdAt: $createdAt,'
      'currentField: $currentField,'
      'currentFieldName: $currentFieldName,'
      'currentFieldId: $currentFieldId,'
      'id: $id,'
      'pointsCurrency: $pointsCurrency,'
      'pointsEarned: $pointsEarned,'
      'status: $status,'
      'survey: $survey,'
      'surveyId: $surveyId,'
      'updatedAt: $updatedAt,'
      'options: $options,'
      '}';

  factory SurveyResult.fromJson(Map<String, dynamic> json) {
    return SurveyResult(
      createdAt: json['createdAt'] as int,
      currentField: json['currentField'] as dynamic,
      currentFieldName: json['currentField']['name'] as dynamic,
      currentFieldId: json['currentFieldId'] as String,
      id: json['id'] as String,
      pointsCurrency: json['pointsCurrency'] as String,
      pointsEarned: json['pointsEarned'] as int,
      status: json['status'] as String,
      survey: json['survey'] as dynamic,
      surveyId: json['surveyId'] as String,
      updatedAt: json['updatedAt'] as int,
      options: json['currentField']['options'] as List<dynamic>,
    );
  }
}

Future<int> getOptionsIndex(option) async {
  dynamic index;

  option.forEach((val, i) => index = i);

  return index;
}

class FieldOption extends Equatable{
  final String action;
  final String id;
  final String name;
  final int score;

  const FieldOption({this.action, this.id, this.name, this.score});

  @override
  List<Object> get props => [
    action,
    id,
    name,
    score,
  ];

  @override
  String toString() => 'FieldOption { '
      'action: $action,'
      'id: $id,'
      'name: $name,'
      'score: $score,'
      '}';

  factory FieldOption.fromJson(Map<String, dynamic> json) {
    return FieldOption(
      action: json['action'] as String,
      id: json['id'] as String,
      name: json['name'] as String,
      score: json['score'] as int,
    );
  }
}