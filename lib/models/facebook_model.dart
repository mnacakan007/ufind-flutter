import 'package:equatable/equatable.dart';

class Facebook extends Equatable {
  final String firstName;
  final String lastName;
  final String picture;

  const Facebook({this.firstName, this.lastName, this.picture});

  @override
  List<Object> get props => [
    firstName,
    lastName,
    picture
  ];

  @override
  String toString() => 'Facebook { '
      'firstName: $firstName,'
      'lastName: $lastName,'
      'picture: $picture,'
      '}';

  factory Facebook.fromJson(Map<String, dynamic> json) {
    return Facebook(
      firstName: json['first_name'] as String,
      lastName: json['last_name'] as String,
      picture: json['picture']['data']['url'] as String,
    );
  }
}