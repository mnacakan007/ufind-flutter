import 'package:equatable/equatable.dart';

class Offer extends Equatable {
  final int amount;
  final dynamic availableDates;
  final int claimed;
  final int createdAt;
  final String currency;
  final String description;
  final int discount;
  final String discountType;
  final String featuredImageUrl;
  final String id;
  final String link;
  final dynamic partner;
  final String partnerId;
  final int price;
  final String title;
  final int updatedAt;
  final int viewsCount;
  final dynamic workingDates;
  final double lat;
  final double lng;
  final String partnerName;
  final String partnerState;
  final dynamic locationCoords;

  const Offer({
    this.amount,
    this.availableDates,
    this.claimed,
    this.createdAt,
    this.currency,
    this.description,
    this.discount,
    this.discountType,
    this.featuredImageUrl,
    this.id,
    this.link,
    this.partner,
    this.partnerId,
    this.price,
    this.title,
    this.updatedAt,
    this.viewsCount,
    this.workingDates,
    this.lat,
    this.lng,
    this.partnerName,
    this.partnerState,
    this.locationCoords
  });

  @override
  List<Object> get props => [
    amount,
    availableDates,
    claimed,
    createdAt,
    currency,
    description,
    discount,
    discountType,
    featuredImageUrl,
    id,
    link,
    partner,
    partnerId,
    price,
    title,
    updatedAt,
    viewsCount,
    workingDates,
    lat,
    lng,
    partnerName,
    partnerState,
    locationCoords
  ];

  @override
  String toString() => 'OffersResult { '
      'amount: $amount,'
      'availableDates: $availableDates,'
      'claimed: $claimed,'
      'createdAt: $createdAt,'
      'currency: $currency,'
      'description: $description,'
      'discount: $discount,'
      'discountType: $discountType,'
      'featuredImageUrl: $featuredImageUrl,'
      'id: $id,'
      'link: $link,'
      'partner: $partner,'
      'partnerId: $partnerId,'
      'price: $price,'
      'title: $title,'
      'updatedAt: $updatedAt,'
      'viewsCount: $viewsCount,'
      'workingDates: $workingDates,'
      'lat: $lat,'
      'lng: $lng,'
      'partnerName: $partnerName,'
      'partnerState: $partnerState,'
      'locationCoords: $locationCoords,'
      '}';

  factory Offer.fromJson(Map<String, dynamic> json) {
    return Offer(
      amount: json['amount'] as int,
      availableDates: json['availableDates'] as dynamic,
      claimed: json['claimed'] as int,
      createdAt: json['createdAt'] as int,
      currency: json['currency'] as String,
      description: json['description'] as String,
      discount: json['discount'] as int,
      discountType: json['discountType'] as String,
      featuredImageUrl: json['featuredImageUrl'] as String,
      id: json['id'] as String,
      link: json['link'] as String,
      partner: json['partner'] as dynamic,
      partnerId: json['partnerId'] as String,
      price: json['price'] as int,
      title: json['title'] as String,
      updatedAt: json['updatedAt'] as int,
      viewsCount: json['viewsCount'] as int,
      workingDates: json['workingDates'] as dynamic,
      lat: json['partner']['address']['lat'] as double,
      lng: json['partner']['address']['lng'] as double,
      partnerName: json['partner']['name'] as String,
      partnerState: json['partner']['address']['state'] as String,
    );
  }
}
