import 'package:equatable/equatable.dart';

class SurveyAnswer extends Equatable{
  final int createdAt;
  final String currentFieldId;
  final String id;
  final String pointsCurrency;
  final int pointsEarned;
  final String status;
  final String surveyId;
  final int updatedAt;

  SurveyAnswer({
    this.createdAt,
    this.currentFieldId,
    this.id,
    this.pointsCurrency,
    this.pointsEarned,
    this.status,
    this.surveyId,
    this.updatedAt
  });

  @override
  List<Object> get props => [
    createdAt,
    currentFieldId,
    id,
    pointsCurrency,
    pointsEarned,
    status,
    surveyId,
    updatedAt
  ];

  @override
  String toString() => 'SurveyAnswer { '
      'createdAt: $createdAt,'
      'currentFieldId: $currentFieldId,'
      'id: $id,'
      'pointsCurrency: $pointsCurrency,'
      'pointsEarned: $pointsEarned,'
      'status: $status,'
      'surveyId: $surveyId,'
      'updatedAt: $updatedAt,'
      '}';

  factory SurveyAnswer.fromJson(Map<String, dynamic> json) {
    return SurveyAnswer(
      createdAt: json['createdAt'] as int,
      currentFieldId: json['currentFieldId'] as String,
      id: json['id'] as String,
      pointsCurrency: json['pointsCurrency'] as String,
      pointsEarned: json['pointsEarned'] as int,
      status: json['status'] as String,
      surveyId: json['surveyId'] as String,
      updatedAt: json['updatedAt'] as int,
    );
  }

}