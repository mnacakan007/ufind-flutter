import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class Quiz extends Equatable {
  final int createdAt;
  final String featuredImageUrl;
  final String id;
  final String partnerId;
  final dynamic result;
  final String title;
  final int totalPointsMax;
  final int totalPointsMin;
  final int updatedAt;

  const Quiz({
    this.createdAt,
    this.featuredImageUrl,
    @required this.id,
    this.partnerId,
    this.result,
    this.title,
    this.totalPointsMax,
    this.totalPointsMin,
    this.updatedAt,
  });

  @override
  List<Object> get props => [
    createdAt,
    featuredImageUrl,
    id,
    partnerId,
    result,
    title,
    totalPointsMax,
    totalPointsMin,
    updatedAt,
  ];

  @override
  String toString() => 'QuizResult { '
      'createdAt: $createdAt,'
      'featuredImageUrl: $featuredImageUrl,'
      'id: $id,'
      'partnerId: $partnerId,'
      'result: $result,'
      'title: $title,'
      'totalPointsMax: $totalPointsMax,'
      'totalPointsMin: $totalPointsMin,'
      'updatedAt: $updatedAt,'
      '}';

  factory Quiz.fromJson(Map<String, dynamic> json) {
    return Quiz(
      createdAt: json['createdAt'] as int,
      featuredImageUrl: json['featuredImageUrl'] as String,
      id: json['id'] as String,
      partnerId: json['partnerId'] as String,
      result: json['result'] as dynamic,
      title: json['title'] as String,
      totalPointsMax: (json['totalPointsMax'] as num).toInt(),
      totalPointsMin: (json['totalPointsMin'] as num).toInt(),
      updatedAt: json['updatedAt'] as int,
    );
  }
}
