import 'package:equatable/equatable.dart';

class Customer extends Equatable {
  final int createdAt;
  final String dateOfBirth;
  final String emailAddress;
  final String firstName;
  final String gender;
  final String id;
  final String lastName;
  final String phoneNumber;
  final String photoUrl;
  final String refererID;
  final String status;
  final int updatedAt;
  final String username;

  const Customer({this.createdAt, this.dateOfBirth, this.emailAddress, this.firstName, this.gender, this.id, this.lastName, this.phoneNumber, this.photoUrl, this.refererID, this.status, this.updatedAt, this.username});

  @override
  List<Object> get props => [
    createdAt, dateOfBirth, emailAddress, firstName, gender, id, lastName, phoneNumber, photoUrl, refererID, status, updatedAt, username
  ];

  @override
  String toString() => 'Customer { '
      'createdAt: $createdAt,'
      'dateOfBirth: $dateOfBirth,'
      'emailAddress: $emailAddress,'
      'firstName: $firstName,'
      'gender: $gender,'
      'id: $id,'
      'lastName: $lastName,'
      'phoneNumber: $phoneNumber,'
      'photoUrl: $photoUrl,'
      'refererID: $refererID,'
      'status: $status,'
      'updatedAt: $updatedAt,'
      'username: $username,'
      '}';

  factory Customer.fromJson(Map<String, dynamic> json) {
    return Customer(
      createdAt: json['createdAt'] as int,
      dateOfBirth: json['dateOfBirth'] as String,
      emailAddress: json['emailAddress'] as String,
      firstName: json['firstName'] as String,
      gender: json['gender'] as String,
      id: json['id'] as String,
      lastName: json['lastName'] as String,
      phoneNumber: json['phoneNumber'] as String,
      photoUrl: json['photoUrl'] as String,
      refererID: json['refererID'] as String,
      status: json['status'] as String,
      updatedAt: json['updatedAt'] as int,
      username: json['username'] as String,
    );
  }
}