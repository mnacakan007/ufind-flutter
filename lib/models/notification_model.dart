import 'package:equatable/equatable.dart';

class NotificationModel extends Equatable {
  final int createdAt;
  final String details;
  final String id;

  const NotificationModel({this.createdAt, this.details, this.id});

  @override
  List<Object> get props => [
     createdAt,
     details,
     id,
  ];

  @override
  String toString() => 'NotificationModel { '
      'createdAt: $createdAt,'
      'details: $details,'
      'id: $id,'
      '}';

  factory NotificationModel.fromJson(Map<String, dynamic> json) {
    return NotificationModel(
      createdAt: json['createdAt'] as int,
      details: json['details'] as String,
      id: json['id'] as String,
    );
  }

}
