import 'package:equatable/equatable.dart';


class LeaderBoard extends Equatable{
  final int place;
  final int score;
  final List<dynamic> topUsers;

  const LeaderBoard({ this.place, this.score, this.topUsers });

  @override
  List<Object> get props => [ place, score, topUsers ];

  @override
  String toString() => 'LeaderBoard { '
      'place: $place,'
      'score: $score,'
      'topUsers: $topUsers,'
      '}';

  factory LeaderBoard.fromJson(Map<String, dynamic> json) {
    return LeaderBoard(
      place: json['place'] as int,
      score: json['score'] as int,
      topUsers: json['topUsers'] as List<dynamic>,
    );
  }
}

class LeaderBoardUser extends Equatable{
  final String name;
  final int place;
  final dynamic score;

  const LeaderBoardUser({ this.name, this.place, this.score });

  @override
  List<Object> get props => [ name, place, score ];

  @override
  String toString() => 'LeaderBoardUser { '
      'name: $name,'
      'place: $place,'
      'score: $score,'
      '}';

  factory LeaderBoardUser.fromJson(Map<String, dynamic> json) {
    return LeaderBoardUser(
      name: json['name'] as String,
      place: json['place'] as int,
      score: json['score'] as dynamic,
    );
  }
}

class Balance extends Equatable{
  final int amount;
  final String currency;

  const Balance({ this.amount, this.currency });

  @override
  List<Object> get props => [ amount, currency ];

  @override
  String toString() => 'Balance { '
      'amount: $amount,'
      'currency: $currency,'
      '}';

  factory Balance.fromJson(Map<String, dynamic> json) {
    return Balance(
      amount: json['amount'] as int,
      currency: json['currency'] as String,
    );
  }

}

class Transaction extends Equatable{
  final int amount;
  final int createdAt;
  final String currency;
  final String details;
  final String id;

  const Transaction({ this.amount, this.createdAt ,this.currency, this.details, this.id });

  @override
  List<Object> get props => [ amount, createdAt, currency, details, id ];

  @override
  String toString() => 'Transaction { '
      'amount: $amount,'
      'createdAt: $createdAt,'
      'currency: $currency,'
      'details: $details,'
      'id: $id,'
      '}';

  factory Transaction.fromJson(Map<String, dynamic> json) {
    return Transaction(
      amount: json['amount'] as int,
      createdAt: json['createdAt'] as int,
      currency: json['currency'] as String,
      details: json['details'] as String,
      id: json['id'] as String,
    );
  }

}

class Order extends Equatable{
  final int createdAt;
  final int discount;
  final String discountType;
  final String id;
  final dynamic offer;
  final String offerId;
  final String qrUrl;
  final String status;
  final int updatedAt;
  final List<dynamic> valid;
  final List<dynamic> availableDates;

  const Order({this.createdAt, this.discount, this.discountType, this.id, this.offer, this.offerId, this.qrUrl, this.status, this.updatedAt, this.valid, this.availableDates });

  @override
  List<Object> get props => [ createdAt, discount, discountType, id, offer, offerId, qrUrl, status, updatedAt, valid, availableDates ];

  @override
  String toString() => 'Order { '
      'createdAt: $createdAt,'
      'discount: $discount,'
      'discountType: $discountType,'
      'id: $id,'
      'offer: $offer,'
      'offerId: $offerId,'
      'qrUrl: $qrUrl,'
      'status: $status,'
      'updatedAt: $updatedAt,'
      'valid: $valid,'
      'availableDates: $availableDates,'
      '}';

  factory Order.fromJson(Map<String, dynamic> json) {
    return Order(
      createdAt: json['createdAt'] as int,
      discount: json['discount'] as int,
      discountType: json['discountType'] as String,
      id: json['id'] as String,
      offer: json['offer'] as dynamic,
      offerId: json['offerId'] as String,
      qrUrl: json['qrUrl'] as String,
      status: json['status'] as String,
      updatedAt: json['updatedAt'] as int,
      valid: json['valid'] as List<dynamic>,
      availableDates: json['offer']['availableDates'] as List<dynamic>,
    );
  }

}