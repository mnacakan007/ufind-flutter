import 'package:equatable/equatable.dart';

class Credentials extends Equatable {
  final int createdAt;
  final dynamic data;
  final String identifier;
  final String provider;
  final String referrer;

  const Credentials({this.createdAt, this.data, this.identifier, this.provider, this.referrer});

  @override
  List<Object> get props => [
    createdAt,
    data,
    identifier,
    provider,
    referrer,
  ];

  @override
  String toString() => 'Credentials { '
      'createdAt: $createdAt,'
      'data: $data,'
      'identifier: $identifier,'
      'provider: $provider,'
      'referrer: $referrer,'
      '}';

  factory Credentials.fromJson(Map<String, dynamic> json) {
    return Credentials(
      createdAt: json['createdAt'] as int,
      data: json['data'] as dynamic,
      identifier: json['identifier'] as String,
      provider: json['provider'] as String,
      referrer: json['referrer'] as String,
    );
  }

}

class TokenData extends Equatable {
  final bool isNew;
  final String token;

  const TokenData({this.isNew, this.token});

  @override
  List<Object> get props => [
    isNew,
    token,
  ];

  @override
  String toString() => 'TokenData { '
      'isNew: $isNew,'
      'token: $token,'
      '}';

  factory TokenData.fromJson(Map<String, dynamic> json) {
    return TokenData(
      isNew: json['isNew'] as bool,
      token: json['token'] as String,
    );
  }
}
