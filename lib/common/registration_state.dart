import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/registration/registr_bloc.dart';
import 'package:ufind_flutter/nav/drawer_menu.dart';
import 'package:ufind_flutter/screens/registration_screen.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  CustomerInfoLoaded customerInfoLoaded;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.bloc<RegistrationBloc>().add(GetMeStarted());
  }

//  @override
//  void dispose(){
//    context.bloc<RegistrationBloc>().distinct();
//    super.dispose();
//  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<RegistrationBloc, RegistrationState>(
        builder: (context, state) {
          return Container(
            child: _buildBody(state),
          );
        },
      ),
    );
  }

  _buildBody(RegistrationState state) {
    if(state is CustomerInfoLoaded) {
      customerInfoLoaded = state;
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is CustomerMeLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is CustomerMeLoaded) {
       if (state.customer == null && customerInfoLoaded != null) {
        return RegistrationScreen(customerInfoLoaded);
      } else {
        return MainWidget();
      }
    } else if (state is CustomerMeError) {
      return Center(
        child: Text(
          'Error loading CustomerMeError!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

}
