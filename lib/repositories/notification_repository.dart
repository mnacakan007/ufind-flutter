import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/notification_model.dart';
import 'base_data_repository.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;

class NotificationRepository extends BaseNotificationRepository {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final http.Client _httpClient;

  NotificationRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();

  @override
  Future<List<NotificationModel>> getNotifications() async {
    List<NotificationModel> notifications = [];
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/me/notifications';

    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      if (response.statusCode == 200 || response.statusCode == 202) {

        List<dynamic> notificationList = json.decode(response.body);
        notificationList.forEach(
              (json) => notifications.add(NotificationModel.fromJson(json)),
        );
      }
      return notifications;
    } catch (err) {
      throw err;
    }
  }

  @override
  void dispose() {
    _httpClient.close();
  }
}
