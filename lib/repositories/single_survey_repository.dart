import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/single_survey_model.dart';
import 'package:ufind_flutter/models/survey_answer_model.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;
import 'base_data_repository.dart';

class SingleSurveyRepository extends BaseSingleSurveyRepository {
  // String token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjM3NzJhNDBhLTVkOTEtNDdjNC05NDYwLWFhYzFmZDkxODk3ZCIsImlkZW50aWZpZXIiOiJtbm9Ad2l0Y2hlcnkuaW8iLCJleHAiOjE2MjA3NDI2MjMsImlzcyI6ImFtLnVmaW5kIn0.xrQ2H7r1SGojUkQaoAEu_M75pOCwsEbDJ63tpgYSmCY';
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final http.Client _httpClient;

  SingleSurveyRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();

  @override
  Future<SurveyResult> getSingleSurvey({String surveyId}) async {
    SurveyResult surveyResult;
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/surveys/$surveyId/result';
    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      if (response.statusCode == 200) {
        dynamic survey = json.decode(response.body);
        surveyResult = SurveyResult.fromJson(survey);
        getSingleSurveyOption(surveyResult.options);
      }
      return surveyResult;
    } catch (err) {
      throw err;
    }
  }

  Future getSingleSurveyOption(options) async {
      List<dynamic> surveyOptions = [];

       options.forEach(
             (json) => surveyOptions.add(FieldOption.fromJson(json)),
       );
      return surveyOptions;
  }

  @override
  Future<SurveyAnswer> createSurveyAnswer({String surveyId, dynamic answer, String token}) async {
    SurveyAnswer surveyAnswerResult;
    var body = jsonEncode(answer);
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/surveys/$surveyId/answers';
    try {
      final response = await _httpClient.post(
          requestUrl,
          body: body,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );

      if (response.statusCode == 201) {
        dynamic answerData = json.decode(response.body);
        surveyAnswerResult = SurveyAnswer.fromJson(answerData);
      }

      return surveyAnswerResult;
    } catch (err) {
      throw err;
    }
  }


  @override
  void dispose() {
    _httpClient.close();
  }
}
