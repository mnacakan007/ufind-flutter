import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/offers_model.dart';
import 'base_data_repository.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;

class OfferRepository extends BaseOfferRepository {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  static const int perPage = 20;
  static const int limit = 10000;
  static const int offset = 0;

  final http.Client _httpClient;

  OfferRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();

  @override
  Future<List<Offer>> getOffers({int page}) async {
    List<Offer> offers = [];
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/offers';

    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      if (response.statusCode == 200) {

        List<dynamic> offerList = json.decode(response.body);
        offerList.forEach(
              (json) => offers.add(Offer.fromJson(json)),
        );
      }
      return offers;
    } catch (err) {
      throw err;
    }
  }

  @override
  void dispose() {
    _httpClient.close();
  }
}
