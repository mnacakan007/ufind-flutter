import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/quiz_model.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;
import 'base_data_repository.dart';

class QuizRepository extends BaseQuizRepository {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  static const int perPage = 20;
  static const int limit = 10000;
  static const int offset = 0;

  final http.Client _httpClient;

  QuizRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();

  @override
  Future<List<Quiz>> getQuizzes({int page, String token}) async {
    List<Quiz> quizzes = [];
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/quizzes';
    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      if (response.statusCode == 200) {

        List<dynamic> quizList = json.decode(response.body);

        quizList.forEach(
              (json) => quizzes.add(Quiz.fromJson(json)),
        );
      }
      return quizzes;
    } catch (err) {
      throw err;
    }
  }

  @override
  void dispose() {
    _httpClient.close();
  }
}
