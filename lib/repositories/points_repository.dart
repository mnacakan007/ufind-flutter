import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/points_model.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;
import 'base_data_repository.dart';

class PointsRepository extends BasePointsRepository {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final http.Client _httpClient;

  PointsRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();

  @override
  Future<LeaderBoard> getLeaderboard({String surveyId}) async {
    LeaderBoard leaderBoard;
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl;

    if(surveyId != null) {
      requestUrl = '${api.baseUrl}/leaderboard?surveyId=$surveyId';
    } else {
      requestUrl = '${api.baseUrl}/leaderboard';
    }

    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );

      if (response.statusCode == 200) {
        prefs.remove('leaderBoard');
        prefs.setString('leaderBoard', response.body);
        dynamic leaderBoardData = json.decode(response.body);
        leaderBoard = LeaderBoard.fromJson(leaderBoardData);
      }

      return leaderBoard;
    } catch (err) {
      throw err;
    }
  }

  @override
  Future<List<Order>> getCurrentUserOrders() async {
    List<Order> orders = [];
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/orders';

    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );

      if (response.statusCode == 200) {
        prefs.remove('orders');
        prefs.setString('orders', response.body);
        List<dynamic> orderList = json.decode(response.body);
        orderList.forEach(
              (json) => orders.add(Order.fromJson(json)),
        );
      }

      return orders;
    } catch (err) {
      throw err;
    }
  }

  @override
  Future<Balance> getBalances() async {
    Balance balance;
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/me/balances';

    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );

      if (response.statusCode == 200) {

        dynamic balanceData = json.decode(response.body);
        balance = Balance.fromJson(balanceData);
      }

      return balance;
    } catch (err) {
      throw err;
    }
  }

  @override
  Future<List<Transaction>> getTransactions() async {
    List<Transaction> transactions = [];
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/me/transactions';

    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );

      if (response.statusCode == 200) {
        prefs.remove('transactions');
        prefs.setString('transactions', response.body);
        List<dynamic> transactionsList = json.decode(response.body);
        transactionsList.forEach(
              (json) => transactions.add(Transaction.fromJson(json)),
        );
      }

      return transactions;
    } catch (err) {
      throw err;
    }
  }

  @override
  void dispose() {
    _httpClient.close();
  }
}
