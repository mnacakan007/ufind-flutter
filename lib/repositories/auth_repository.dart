import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:ufind_flutter/models/auth_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'base_data_repository.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;

class AuthRepository extends BaseAuthRepository {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  TokenData authResult;
  final http.Client _httpClient;

  AuthRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();

  @override
  Future<TokenData> createToken({dynamic credentials}) async {
    var body = jsonEncode(credentials);
    print('body: $body');
    final SharedPreferences prefs = await _prefs;
    String requestUrl = '${api.baseUrl}/auth/token';

    try {
      final response = await _httpClient.post(
          requestUrl,
          body: body,
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      print('createToken statusCode: ${response.statusCode}');

      if (response.statusCode == 201) {
        dynamic tokenData = json.decode(response.body);
        authResult = TokenData.fromJson(tokenData);
        prefs.setString('token', authResult.token);
        print('authResult: ${authResult.token}');
      }

      return authResult;
    } catch (err) {
      throw err;
    }
  }

  @override
  void dispose() {
    _httpClient.close();
  }

  Future<void> deleteToken() async {
    await Future.delayed(Duration(seconds: 1));
    authResult = null;
    return authResult;
  }

  Future<TokenData> authenticate() async {
    await Future.delayed(Duration(seconds: 1));
    return authResult;
  }



}
