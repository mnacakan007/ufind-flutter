import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/customerInfo_model.dart';
import 'package:ufind_flutter/models/facebook_model.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;
import 'base_data_repository.dart';

class CustomerRepository extends BaseCustomerRepository {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final http.Client _httpClient;

  CustomerRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();


  @override
  Future<Customer> getMe() async {
    Customer customer;
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/me';

    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      print('getMe ${response.statusCode}');
      if (response.statusCode == 200) {
        dynamic c = json.decode(response.body);
        customer = Customer.fromJson(c);
        print('customer $customer');
        prefs.setBool('registration', true);
      } else if (response.statusCode != 401 && response.statusCode == 500) {
        prefs.setBool('registration', false);
        print('not registration ${response.body}');
      }
      return customer;
    } catch (err) {
      throw err;
    }
  }

  @override
  Future<Facebook> getCustomerInfo({String customerToken, String userId}) async {
    Facebook customerInfo;
    final SharedPreferences prefs = await _prefs;

    try {
      final response = await _httpClient.get('https://graph.facebook.com/$userId?fields=id,first_name,last_name,gender,picture,birthday&type=large&access_token=$customerToken}',
          headers: {
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      if (response.statusCode == 200) {
        dynamic facebook = json.decode(response.body);
        customerInfo = Facebook.fromJson(facebook);
        prefs.setString('customerInfo', response.body);
      }
      return customerInfo;
    } catch (err) {
      throw err;
    }
  }

  @override
  Future<Customer> createUser({dynamic customer}) async {
    Customer user;
    final SharedPreferences prefs = await _prefs;
    prefs.setString('token', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjM3NzJhNDBhLTVkOTEtNDdjNC05NDYwLWFhYzFmZDkxODk3ZCIsImlkZW50aWZpZXIiOiJtbm9Ad2l0Y2hlcnkuaW8iLCJleHAiOjE2MjE1MjAyNjksImlzcyI6ImFtLnVmaW5kIn0.dxkSu09kEhyNsr2NK9EPLNEDoqZlf5igzFRLoh2xRHw');
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/me';
    var customerForm = jsonEncode(customer);
    print('customerForm $customerForm');
    try {
      final response = await _httpClient.post(requestUrl,
          body: customerForm,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      print('user statusCode ${response.statusCode}');
      if (response.statusCode == 200) {
        dynamic c = json.decode(response.body);
        user = Customer.fromJson(c);
        print('user $user');
      }

      return user;
    } catch (err) {
      throw err;
    }
  }

  @override
  Future<Customer> updateUser({dynamic customer}) async {
    Customer user;
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/me';
    var customerForm = jsonEncode(customer);
    print('customerForm $customerForm');
    try {
      final response = await _httpClient.put(requestUrl,
          body: customerForm,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      print('user statusCode ${response.statusCode}');
      if (response.statusCode == 200) {
        dynamic c = json.decode(response.body);
        user = Customer.fromJson(c);
        print('user $user');
      }

      return user;
    } catch (err) {
      throw err;
    }
  }

  @override
  void dispose() {
    _httpClient.close();
  }
}
