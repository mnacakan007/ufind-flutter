import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/offers_model.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;
import 'base_data_repository.dart';

class SingleOfferRepository extends BaseSingleOfferRepository {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final http.Client _httpClient;

  SingleOfferRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();

  @override
  Future<Offer> getOfferById({String offerId}) async {
    Offer offerResult;
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/offers/$offerId';
    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      if (response.statusCode == 200) {
        dynamic offer = json.decode(response.body);
        offerResult = Offer.fromJson(offer);
      }
      return offerResult;
    } catch (err) {
      throw err;
    }
  }

  @override
  void dispose() {
    _httpClient.close();
  }
}
