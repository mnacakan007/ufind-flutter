import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/customerInfo_model.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;

import 'base_data_repository.dart';

class ProfileRepository extends BaseProfileRepository {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  final http.Client _httpClient;

  ProfileRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();


  @override
  Future<Customer> getMe() async {
    Customer customer;
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/me';

    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      print('getMe ${response.statusCode}');
      if (response.statusCode == 200) {
        dynamic c = json.decode(response.body);
        customer = Customer.fromJson(c);
        print('customer $customer');
        prefs.setBool('registration', true);
      } else if (response.statusCode != 401 && response.statusCode == 500) {
        prefs.setBool('registration', false);
        print('not registration ${response.body}');
      }
      return customer;
    } catch (err) {
      throw err;
    }
  }

  @override
  void dispose() {
    _httpClient.close();
  }
}
