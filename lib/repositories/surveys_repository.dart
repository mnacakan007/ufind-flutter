import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/survey_model.dart';
import 'base_data_repository.dart';
import 'package:ufind_flutter/environments/environment.dart' as api;

class SurveyRepository extends BaseSurveyRepository {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  static const int perPage = 20;
  static const int limit = 10000;
  static const int offset = 0;

  final http.Client _httpClient;

  SurveyRepository({http.Client httpClient})
      : _httpClient = httpClient ?? http.Client();

  @override
  Future<List<Survey>> getSurveys({int page}) async {
    List<Survey> surveys = [];
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');
    String requestUrl = '${api.baseUrl}/surveys';

    try {
      final response = await _httpClient.get(requestUrl,
          headers: {
            HttpHeaders.authorizationHeader: 'Bearer $token',
            HttpHeaders.contentTypeHeader: 'application/json',
          }
      );
      if (response.statusCode == 200) {

        List<dynamic> surveyList = json.decode(response.body);

        surveyList.forEach(
          (json) => surveys.add(Survey.fromJson(json)),
        );
      }
      return surveys;
    } catch (err) {
      throw err;
    }
  }

  @override
  void dispose() {
    _httpClient.close();
  }
}
