import 'package:ufind_flutter/models/auth_model.dart';
import 'package:ufind_flutter/models/customerInfo_model.dart';
import 'package:ufind_flutter/models/offers_model.dart';
import 'package:ufind_flutter/models/points_model.dart';
import 'package:ufind_flutter/models/quiz_model.dart';
import 'package:ufind_flutter/models/single_survey_model.dart';
import 'package:ufind_flutter/models/survey_answer_model.dart';
import 'package:ufind_flutter/models/survey_model.dart';
import 'package:ufind_flutter/models/notification_model.dart';

abstract class BaseSurveyRepository {
  Future<List<Survey>> getSurveys({int page});
  void dispose();
}

abstract class BaseSingleSurveyRepository {
  Future<SurveyResult> getSingleSurvey({String surveyId});
  Future<SurveyAnswer> createSurveyAnswer({String surveyId, SurveyAnswer answer});
  void dispose();
}

abstract class BaseQuizRepository {
  Future<List<Quiz>> getQuizzes({int page});
  void dispose();
}

abstract class BaseAuthRepository {
  Future<TokenData> createToken({dynamic credentials});
  void dispose();
}
abstract class BaseOfferRepository {
  Future<List<Offer>> getOffers({int page});
  void dispose();
}

abstract class BaseSingleOfferRepository {
  Future<Offer> getOfferById({String offerId});
  void dispose();
}

abstract class BasePointsRepository {
  Future<LeaderBoard> getLeaderboard({String surveyId});
  Future<List<Order>> getCurrentUserOrders();
  Future<Balance> getBalances();
  Future<List<Transaction>> getTransactions();
  void dispose();
}

abstract class BaseNotificationRepository {
  Future<List<NotificationModel>> getNotifications();
  void dispose();
}

abstract class BaseCustomerRepository {
  Future<dynamic> getCustomerInfo({String customerToken, String userId});
  Future<Customer> createUser({Customer customer});
  Future<Customer> updateUser({Customer customer});
  Future<Customer> getMe();
  void dispose();
}

abstract class BaseProfileRepository {
  Future<Customer> getMe();
  void dispose();
}