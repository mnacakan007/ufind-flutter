import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:ufind_flutter/blocs/auth/auth_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/screens/profile_screen.dart';
import 'package:ufind_flutter/utils/class_builder.dart';

import 'navigation.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['profile', 'email']);

// ignore: must_be_immutable
class MainWidget extends StatefulWidget {
  @override
  _MainWidgetState createState() => _MainWidgetState();
}

class _MainWidgetState extends State<MainWidget> with TickerProviderStateMixin {
  KFDrawerController _drawerController;
  static final FacebookLogin facebookSignIn = new FacebookLogin();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  GoogleSignInAccount _currentUser;

  @override
  void initState() {
    super.initState();

    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account){
      setState(() {
        _currentUser = account;
      });
    });

    _googleSignIn.signInSilently();

    _drawerController = KFDrawerController(
      initialPage: ClassBuilder.fromString('NavigationWidget'),
      items: [
//        KFDrawerItem.initWithPage(
//          text: Text('Mno', style: TextStyle(color: Colors.white, fontSize: 26.0)),
//        ),
        KFDrawerItem.initWithPage(
          text: Text('Գլխավոր', style: TextStyle(color: Colors.white)),
          icon: Icon(Icons.home, color: Colors.white),
          page: NavigationWidget(),
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            'Անձնական տվյալներ',
            style: TextStyle(color: Colors.white),
          ),
          icon: Icon(Icons.person, color: Colors.white),
          page: ProfileScreen(),
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            'Հրավիրել ընկերոջը',
            style: TextStyle(color: Colors.white),
          ),
          icon: Icon(Icons.add_to_home_screen, color: Colors.white),
//          page: ClassBuilder.fromString('SettingsPage'),
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            'Միանալ հարցմանը',
            style: TextStyle(color: Colors.white),
          ),
          icon: Icon(Icons.queue_play_next, color: Colors.white),
//          page: ClassBuilder.fromString('SettingsPage'),
        ),
        KFDrawerItem.initWithPage(
          text: Text(
            'Հավելվածի մասին',
            style: TextStyle(color: Colors.white),
          ),
          icon: Icon(Icons.info_outline, color: Colors.white),
//          page: ClassBuilder.fromString('SettingsPage'),
        ),
      ],
    );

  }

  Future<Null> _logOut() async {
    await facebookSignIn.logOut();

    _googleSignIn.signOut();
    _googleSignIn.disconnect();

    final SharedPreferences prefs = await _prefs;
    prefs.remove('token');
    prefs.remove('registration');
    context.bloc<AuthBloc>().add(AuthLoggedOut());
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: KFDrawer(
        controller: _drawerController,
        header: Column(
          children: <Widget>[
            Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0),
                  width: MediaQuery.of(context).size.width * 0.35,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100.0),
                    child: Image.asset(
                      'assets/logo.png',
                      alignment: Alignment.centerLeft,
                    ),
                  ),
                )),
            SizedBox(height: 15.0),
            _currentUser != null ? Align(
              alignment: Alignment.centerLeft,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage:NetworkImage(_currentUser.photoUrl),
                      radius: 22.0,
                    ),
                    title: Text(_currentUser.displayName ?? ''),
                    subtitle: Text(_currentUser.email ?? ''),
                  ),
                ],
              ),
            ): Container()
          ],
        ),
        footer: KFDrawerItem(
          text: Text(
            'Ելք',
            style: TextStyle(color: Colors.white),
          ),
          icon: Icon(
            Icons.input,
            color: Colors.white,
          ),
          onPressed: _logOut
        ),
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              Color.fromRGBO(255, 255, 255, 1.0),
              Color.fromRGBO(44, 72, 171, 1.0)
            ],
            tileMode: TileMode.repeated,
          ),
        ),
      ),
    );
  }
}

