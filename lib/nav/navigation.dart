import 'dart:ui';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:ufind_flutter/blocs/notifications/notifications_bloc.dart';
import 'package:ufind_flutter/nav/tabs.dart';
import 'package:ufind_flutter/screens/notification_screen.dart';
import 'package:ufind_flutter/screens/points/points_screen.dart';
import 'package:ufind_flutter/screens/quizzes_screen.dart';
import 'package:ufind_flutter/screens/surveys_screen.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';


// ignore: must_be_immutable
class NavigationWidget extends KFDrawerContent {
  @override
  _NavigationWidgetState createState() => _NavigationWidgetState();
}

class _NavigationWidgetState extends State<NavigationWidget> {
  final String serverToken = '<Server-Token>';
  final FirebaseMessaging firebaseMessaging = FirebaseMessaging();
  String _homeScreenText = "Waiting for token...";
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  int _selectedIndex = 0;
  List<String> titleList = [
    'Հարցումներ',
    'Մրցե՞նք',
    'Առաջարկներ',
    'Միավորներ',
  ];

  @override
  void initState() {
    super.initState();

    firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(sound: true, badge: true, alert: true));
    firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
      print("Settings registered: $settings");
    });

    firebaseMessaging.getToken().then((String token) {
      assert(token != null);
      setState(() {
        _homeScreenText = "Push Messaging token: $token";
      });
    });

  }

  static const TextStyle optionStyle = TextStyle(
      fontSize: 30, fontWeight: FontWeight.bold, color: Colors.blueAccent);
  static List<Widget> _widgetOptions = <Widget>[
    SurveyScreen(),
    QuizzesScreen(),
    TabScreen(),
    PointsScreen()
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(titleList[_selectedIndex], style: TextStyle(fontWeight: FontWeight.bold)),
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.notifications,
              color: Colors.black,
            ),
            onPressed: () {
              context.bloc<NotificationBloc>().add(NotificationStarted());
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        NotificationScreen()),
              );
            },
          ),
        ],
        leading: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(32.0)),
          child: Material(
            shadowColor: Colors.transparent,
            color: Colors.transparent,
            child: IconButton(
              icon: Icon(
                Icons.menu,
                color: Colors.black,
              ),
              onPressed: widget.onMenuPressed,
            ),
          ),
        ),
      ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
//      drawer: Drawer(
//        child: ListView(
//          padding: EdgeInsets.zero,
//          children: <Widget>[
//            Container(
//              child: DrawerHeader(
//                  child: Text('Mno', style: TextStyle(color: Colors.black, fontSize: 24.0)),
//                  decoration: BoxDecoration(color: Colors.white),
//                  margin: EdgeInsets.all(0.0),
//                  padding: EdgeInsets.all(16.0)),
//            ),
//            ListTile(
//              title: Text('Գլխավոր'),
//              leading: Icon(Icons.home),
//              onTap: () {
//                _onItemTapped(0);
//                Navigator.pop(context);
//              },
//            ),
//            ListTile(
//              title: Text('Անձնական տվյալներ'),
//              leading: Icon(Icons.person),
//              onTap: () {
//                Navigator.pop(context);
//              },
//            ),
//            ListTile(
//              title: Text('Հրավիրել ընկերոջը'),
//              leading: Icon(Icons.add_to_home_screen),
//              onTap: () {
//                Navigator.pop(context);
//              },
//            ),
//            ListTile(
//              title: Text('Միանալ հարցմանը'),
//              leading: Icon(Icons.queue_play_next),
//              onTap: () {
//                Navigator.pop(context);
//              },
//            ),
//            ListTile(
//              title: Text('Հավելվածի մասին'),
//              leading: Icon(Icons.info_outline),
//              onTap: () {
//                Navigator.pop(context);
//              },
//            ),
//            ListTile(
//              title: Text('Ելք'),
//              leading: Icon(Icons.exit_to_app),
//              onTap: () {
//                Navigator.pop(context);
//              },
//            ),
//          ],
//        ),
//      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Հարցում'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.lightbulb_outline),
            title: Text('Մրցե՞նք'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.assignment),
            title: Text('Առաջարկներ'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.monetization_on),
            title: Text('Միավորներ'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blueAccent,
        unselectedItemColor: Colors.black54,
        onTap: _onItemTapped,
      ),
    );
  }
}
