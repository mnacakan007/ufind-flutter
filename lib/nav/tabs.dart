import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/offers/offers_bloc.dart';
import 'package:ufind_flutter/screens/offers_list_screen.dart';
import 'package:ufind_flutter/screens/offers_map_screen.dart';

class TabScreen extends StatefulWidget {
  @override
  _TabScreenState createState() => _TabScreenState();
}

class _TabScreenState extends State<TabScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.bloc<OfferBloc>().add(OfferStarted());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: DefaultTabController(
          length: 2,
          initialIndex: 1,
          child: Scaffold(
            appBar: PreferredSize(
                preferredSize: Size.fromHeight(50.0),
                child: AppBar(
                  title: TabBar(
                    indicatorColor: Colors.lightBlue,
                    tabs: <Widget>[
                      Tab(
                        text: "Գործնկերներ".toUpperCase(),
                      ),
                      Tab(
                        text: "Առաջարկներ".toUpperCase(),
                      )
                    ],
                  ),
                )),
            body: BlocBuilder<OfferBloc, OfferState>(
              builder: (context, state) {
                return Container(
                  child: _buildBody(state),
                );
              },
            ),
          )),
    );
  }

  _buildBody(OfferState state) {
    if (state is OfferLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is OfferLoaded) {
      return TabBarView(
        children: <Widget>[OffersMapScreen(state), OffersListScreen(state)],
      );
    } else if (state is OfferError) {
      return Center(
        child: Text(
          'Error loading offers!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }
}

class AndroidSecondPage extends StatelessWidget {
  const AndroidSecondPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text("Առաջարկներ"),
    );
  }
}
