part of 'quiz_bloc.dart';

abstract class QuizEvent extends Equatable {
  const QuizEvent();

  @override
  List<Object> get props => [];
}

class QuizStarted extends QuizEvent {}

class RefreshQuiz extends QuizEvent {}

class LoadMoreQuiz extends QuizEvent {
  final List<Quiz> quizzes;

  const LoadMoreQuiz({this.quizzes});

  @override
  List<Object> get props => [quizzes];

  @override
  String toString() => 'LoadMoreQuiz { quizzes: $quizzes }';
}
