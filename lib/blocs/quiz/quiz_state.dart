part of 'quiz_bloc.dart';

abstract class QuizState extends Equatable {
  const QuizState();

  @override
  List<Object> get props => [];
}

class QuizEmpty extends QuizState {}

class QuizLoading extends QuizState {}

class QuizLoaded extends QuizState {
  final List<Quiz> quizzes;

  const QuizLoaded({this.quizzes});

  @override
  List<Object> get props => [quizzes];

  @override
  String toString() => 'QuizLoaded { quizzes: $quizzes }';
}

class QuizError extends QuizState {}
