import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ufind_flutter/models/quiz_model.dart';
import 'package:ufind_flutter/repositories/quiz_repository.dart';

part 'quiz_event.dart';
part 'quiz_state.dart';

class QuizBloc extends Bloc<QuizEvent, QuizState> {
  final QuizRepository _quizRepository;

  QuizBloc({@required QuizRepository quizRepository})
      : assert(quizRepository != null),
        _quizRepository = quizRepository;

  @override
  QuizState get initialState => QuizEmpty();

  @override
  Stream<QuizState> mapEventToState(QuizEvent event) async* {
    if (event is QuizStarted) {
      yield* _mapAppStartedToState();
    } else if (event is RefreshQuiz) {
      yield* _getQuizzes(quizzes: []);
    } else if (event is LoadMoreQuiz) {
      yield* _mapLoadMoreQuizzes(event);
    }
  }

  Stream<QuizState> _getQuizzes({List<Quiz> quizzes, int page = 0}) async* {
    // Request surveys
    try {
      List<Quiz> newQuizList =
          quizzes + await _quizRepository.getQuizzes(page: page);
      yield QuizLoaded(quizzes: newQuizList);
    } catch (err) {
      yield QuizError();
    }
  }

  Stream<QuizState> _mapAppStartedToState() async* {
    yield QuizLoading();
    yield* _getQuizzes(quizzes: []);
  }

  Stream<QuizState> _mapLoadMoreQuizzes(LoadMoreQuiz event) async* {
    final int nextPage = event.quizzes.length ~/ QuizRepository.perPage;
    yield* _getQuizzes(quizzes: event.quizzes, page: nextPage);
  }
}
