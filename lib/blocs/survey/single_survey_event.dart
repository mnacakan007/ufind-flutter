part of 'single_survey_bloc.dart';

abstract class SingleSurveyEvent extends Equatable {
  const SingleSurveyEvent();

  @override
  List<Object> get props => [];
}

class SingleSurveyStarted extends SingleSurveyEvent {
  final String surveyId;

  const SingleSurveyStarted({this.surveyId});

  @override
  List<Object> get props => [surveyId];

  @override
  String toString() => 'SingleSurveyStarted { surveyId: $surveyId}';
}

class RefreshSingleSurvey extends SingleSurveyEvent {}

class LoadMoreSingleSurvey extends SingleSurveyEvent {
  final SurveyResult survey;

  const LoadMoreSingleSurvey({this.survey});

  @override
  List<Object> get props => [survey];

  @override
  String toString() => 'LoadMoreSingleSurvey { survey: $survey }';
}

class CreateSurveyAnswer extends SingleSurveyEvent {
  final String surveyId;
  final dynamic answer;

  const CreateSurveyAnswer({this.surveyId, this.answer});

  @override
  List<Object> get props => [surveyId, answer];

  @override
  String toString() => 'CreateSurveyAnswer { surveyId: $surveyId, answer: $answer}';
}
