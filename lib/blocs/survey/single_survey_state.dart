part of 'single_survey_bloc.dart';

abstract class SingleSurveyState extends Equatable {
  const SingleSurveyState();

  @override
  List<Object> get props => [];
}

class SingleSurveyEmpty extends SingleSurveyState {}

class SingleSurveyLoading extends SingleSurveyState {}

class SingleSurveyLoaded extends SingleSurveyState {
  final SurveyResult survey;

  const SingleSurveyLoaded({this.survey});

  @override
  List<Object> get props => [survey];

  @override
  String toString() => 'SingleSurveyLoaded { survey: $survey }';
}

class SingleSurveyError extends SingleSurveyState {}

class SurveyAnswerEmpty extends SingleSurveyState {}

class SurveyAnswerLoading extends SingleSurveyState {}

class SurveyAnswerLoaded extends SingleSurveyState {
  final SurveyAnswer surveyAnswerResult;

  const SurveyAnswerLoaded({this.surveyAnswerResult});

  @override
  List<Object> get props => [surveyAnswerResult];

  @override
  String toString() => 'SurveyAnswerLoaded { surveyAnswerResult: $surveyAnswerResult }';
}

class SurveyAnswerError extends SingleSurveyState {}
