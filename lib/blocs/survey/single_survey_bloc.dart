import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ufind_flutter/models/single_survey_model.dart';
import 'package:ufind_flutter/models/survey_answer_model.dart';
import 'package:ufind_flutter/repositories/single_survey_repository.dart';

part 'single_survey_event.dart';
part 'single_survey_state.dart';

class SingleSurveyBloc extends Bloc<SingleSurveyEvent, SingleSurveyState> {
  final SingleSurveyRepository _singleSurveyRepository;

  SingleSurveyBloc({@required SingleSurveyRepository singleSurveyRepository})
      : assert(singleSurveyRepository != null),
        _singleSurveyRepository = singleSurveyRepository;

  @override
  SingleSurveyState get initialState => SingleSurveyEmpty();

  @override
  Stream<SingleSurveyState> mapEventToState(SingleSurveyEvent event) async* {
    if (event is SingleSurveyStarted) {
      yield* _mapAppStartedToState(event.surveyId);
    } else if (event is RefreshSingleSurvey) {
      yield* _getSurvey(survey: null);
    } else if (event is CreateSurveyAnswer) {
      yield* _mapSurveyAnswerCreate(event.surveyId, event.answer);
    }
  }

  Stream<SingleSurveyState> _getSurvey({SurveyResult survey, String surveyId = ''}) async* {
    // Request survey
    try {
      SurveyResult newSurvey = await _singleSurveyRepository.getSingleSurvey(surveyId: surveyId);
      yield SingleSurveyLoaded(survey: newSurvey);
    } catch (err) {
      yield SingleSurveyError();
    }
  }

  Stream<SingleSurveyState> _mapAppStartedToState(surveyId) async* {
    yield SingleSurveyLoading();
    yield* _getSurvey(survey: null, surveyId: surveyId);
  }


  Stream<SingleSurveyState> _createSurveyAnswer({SurveyAnswer surveyAnswerResult, String surveyId, dynamic answer}) async* {
    // Request survey answer
    try {
      SurveyAnswer newSurveyAnswer = await _singleSurveyRepository.createSurveyAnswer(surveyId: surveyId, answer: answer);
      yield SurveyAnswerLoaded(surveyAnswerResult: newSurveyAnswer);
    } catch (err) {
      yield SurveyAnswerError();
    }
  }

  Stream<SingleSurveyState> _mapSurveyAnswerCreate(surveyId, answer) async* {
    yield SurveyAnswerLoading();
    yield* _createSurveyAnswer(surveyAnswerResult: null, surveyId: surveyId, answer: answer);
  }

}



