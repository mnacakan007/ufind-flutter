import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ufind_flutter/models/offers_model.dart';
import 'package:ufind_flutter/repositories/single_offer_repository.dart';

part 'single_offer_event.dart';
part 'single_offer_state.dart';

class SingleOfferBloc extends Bloc<SingleOfferEvent, SingleOfferState> {
  final SingleOfferRepository _singleOfferRepository;

  SingleOfferBloc({@required SingleOfferRepository singleOfferRepository})
      : assert(singleOfferRepository != null),
        _singleOfferRepository = singleOfferRepository;

  @override
  SingleOfferState get initialState => SingleOfferEmpty();

  @override
  Stream<SingleOfferState> mapEventToState(SingleOfferEvent event) async* {
    if (event is SingleOfferStarted) {
      yield* _mapAppStartedToState(event.offerId);
    }
  }

  Stream<SingleOfferState> _getSurvey({Offer offer, String offerId = ''}) async* {
    try {
      Offer newOffer = await _singleOfferRepository.getOfferById(offerId: offerId);
      yield SingleOfferLoaded(offer: newOffer);
    } catch (err) {
      yield SingleOfferError();
    }
  }

  Stream<SingleOfferState> _mapAppStartedToState(offerId) async* {
    yield SingleOfferLoading();
    yield* _getSurvey(offer: null, offerId: offerId);
  }


}



