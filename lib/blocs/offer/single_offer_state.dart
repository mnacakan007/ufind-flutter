part of 'single_offer_bloc.dart';

abstract class SingleOfferState extends Equatable {
  const SingleOfferState();

  @override
  List<Object> get props => [];
}

// Initial State
class SingleOfferEmpty extends SingleOfferState {}

class SingleOfferLoading extends SingleOfferState {}

class SingleOfferLoaded extends SingleOfferState {
  final Offer offer;

  const SingleOfferLoaded({this.offer});

  @override
  List<Object> get props => [offer];

  @override
  String toString() => 'SingleOfferLoaded { offer: $offer }';
}

// API Request Error
class SingleOfferError extends SingleOfferState {}

