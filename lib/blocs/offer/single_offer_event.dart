part of 'single_offer_bloc.dart';

abstract class SingleOfferEvent extends Equatable {
  const SingleOfferEvent();

  @override
  List<Object> get props => [];
}

class SingleOfferStarted extends SingleOfferEvent {
  final String offerId;

  const SingleOfferStarted({this.offerId});

  @override
  List<Object> get props => [offerId];

  @override
  String toString() => 'SingleOfferStarted { offerId: $offerId}';
}


