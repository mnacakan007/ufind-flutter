import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/auth_model.dart';
import 'package:ufind_flutter/repositories/auth_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository _authRepository;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  AuthBloc({@required AuthRepository authRepository})
      : assert(authRepository != null),
        _authRepository = authRepository;

  @override
  AuthState get initialState => AuthEmpty();

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    final SharedPreferences prefs = await _prefs;
    String token = prefs.getString('token');

    if (event is AuthStarted) {

      bool hasToken = false;

      if (token != null) {
        hasToken = true;
      }

      if (hasToken) {
        yield AuthLoaded();
      } else {
        yield AuthError();
      }
    } else if (event is AuthCreateToken) {
      yield* _authAppStartedToState(event.credentials);
    } else if (event is AuthLoggedOut) {
      yield AuthLoading();
      await _authRepository.deleteToken();
      yield AuthError();
    }
  }

  Stream<AuthState> _createToken({TokenData authResult, dynamic credentials}) async* {
    try {
      TokenData newTokenData = await _authRepository.createToken(credentials: credentials);
      yield AuthLoaded(tokenData: newTokenData);
    } catch (err) {
      yield AuthError();
    }
  }

  Stream<AuthState> _authAppStartedToState(credentials) async* {
    yield AuthLoading();
    yield* _createToken(authResult: null, credentials: credentials);
  }
}
