part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();

  @override
  List<Object> get props => [];
}

class AuthStarted extends AuthEvent {}

class AuthCreateToken extends AuthEvent {
  final dynamic credentials;

  const AuthCreateToken({this.credentials});

  @override
  List<Object> get props => [credentials];

  @override
  String toString() => 'AuthCreateToken { credentials: $credentials}';
}

class AuthLoggedOut extends AuthEvent {}