part of 'auth_bloc.dart';

abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];
}

// Initial State
class AuthEmpty extends AuthState {}

class AuthLoading extends AuthState {}

class AuthLoaded extends AuthState {
  final TokenData tokenData;

  const AuthLoaded({this.tokenData});

  @override
  List<Object> get props => [tokenData];

  @override
  String toString() => 'AuthLoaded { tokenData: $tokenData }';
}

class AuthRegistered extends AuthState {
  final bool registration;

  const AuthRegistered({this.registration});

  @override
  List<Object> get props => [registration];

  @override
  String toString() => 'AuthLoaded { registration: $registration }';
}

// API Request Error
class AuthError extends AuthState {}
