part of 'offers_bloc.dart';

abstract class OfferEvent extends Equatable {
  const OfferEvent();

  @override
  List<Object> get props => [];
}

class OfferStarted extends OfferEvent {}

class RefreshOffer extends OfferEvent {}

class LoadMoreOffer extends OfferEvent {
  final List<Offer> offers;

  const LoadMoreOffer({this.offers});

  @override
  List<Object> get props => [offers];

  @override
  String toString() => 'LoadMoreOffer { offers: $offers }';
}
