import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ufind_flutter/models/offers_model.dart';
import 'package:ufind_flutter/repositories/offers_repository.dart';

part 'offers_event.dart';
part 'offers_state.dart';

class OfferBloc extends Bloc<OfferEvent, OfferState> {
  final OfferRepository _offerRepository;

  OfferBloc({@required OfferRepository offerRepository})
      : assert(offerRepository != null),
        _offerRepository = offerRepository;

  @override
  OfferState get initialState => OfferEmpty();

  @override
  Stream<OfferState> mapEventToState(OfferEvent event) async* {
    if (event is OfferStarted) {
      yield* _mapAppStartedToState();
    } else if (event is RefreshOffer) {
      yield* _getOffers(offers: []);
    } else if (event is LoadMoreOffer) {
      yield* _mapLoadMoreOffersToState(event);
    }
  }

  Stream<OfferState> _getOffers({List<Offer> offers, int page = 0}) async* {
    // Request offers
    try {
      List<Offer> newOffersList =
          offers + await _offerRepository.getOffers();
      yield OfferLoaded(offers: newOffersList);
    } catch (err) {
      yield OfferError();
    }
  }

  Stream<OfferState> _mapAppStartedToState() async* {
    yield OfferLoading();
    yield* _getOffers(offers: []);
  }

  Stream<OfferState> _mapLoadMoreOffersToState(LoadMoreOffer event) async* {
    final int nextPage = event.offers.length ~/ OfferRepository.perPage;
    yield* _getOffers(offers: event.offers, page: nextPage);
  }
}
