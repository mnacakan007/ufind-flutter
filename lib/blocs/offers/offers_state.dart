part of 'offers_bloc.dart';

abstract class OfferState extends Equatable {
  const OfferState();

  @override
  List<Object> get props => [];
}

// Initial State
class OfferEmpty extends OfferState {}

class OfferLoading extends OfferState {}

class OfferLoaded extends OfferState {
  final List<Offer> offers;

  const OfferLoaded({this.offers});

  @override
  List<Object> get props => [offers];

  @override
  String toString() => 'OfferLoaded { offers: $offers}';
}

// API Request Error
class OfferError extends OfferState {}
