part of 'points_bloc.dart';

abstract class PointsEvent extends Equatable {
  const PointsEvent();

  @override
  List<Object> get props => [];
}

class PointsStarted extends PointsEvent {
  final String surveyId;

  const PointsStarted({this.surveyId});

  @override
  List<Object> get props => [surveyId];

  @override
  String toString() => 'PointsStarted { surveyId: $surveyId}';
}





