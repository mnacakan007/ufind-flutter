import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ufind_flutter/models/points_model.dart';
import 'package:ufind_flutter/repositories/points_repository.dart';


part 'points_event.dart';
part 'points_state.dart';

class PointsBloc extends Bloc<PointsEvent, PointsState> {
  final PointsRepository _pointsRepository;

  PointsBloc({@required PointsRepository pointsRepository})
      : assert(pointsRepository != null),
        _pointsRepository = pointsRepository;

  @override
  PointsState get initialState => PointsEmpty();

  @override
  Stream<PointsState> mapEventToState(PointsEvent event) async* {
    if (event is PointsStarted) {
      yield* _mapAppStartedToOrdersState();
      yield* _mapAppStartedToTransactionsState();
      yield* _mapAppStartedToLeaderBoardState(event.surveyId);
      yield* _mapAppStartedToBalanceState();
    }
  }


  Stream<PointsState> _getLeaderboard({LeaderBoard leaderBoard, String surveyId = ''}) async* {
    try {
      LeaderBoard newLeaderBoard = await _pointsRepository.getLeaderboard(surveyId: surveyId);
      yield LeaderBoardLoaded(leaderBoard: newLeaderBoard);
    } catch (err) {
      yield LeaderBoardError();
    }
  }

  Stream<PointsState> _mapAppStartedToLeaderBoardState(surveyId) async* {
    yield LeaderBoardLoading();
    yield* _getLeaderboard(leaderBoard: null, surveyId: surveyId);
  }


  Stream<PointsState> _getCurrentUserOrders({List<Order> orders}) async* {
    try {
      List<Order> newOrderList =
      orders + await _pointsRepository.getCurrentUserOrders();
      yield OrdersLoaded(orders: newOrderList);
    } catch (err) {
      yield OrdersError();
    }
  }

  Stream<PointsState> _mapAppStartedToOrdersState() async* {
    yield OrdersLoading();
    yield* _getCurrentUserOrders(orders: []);
  }


  Stream<PointsState> _getTransactions({List<Transaction> transactions}) async* {
    try {
      List<Transaction> newTransactionList =
          transactions + await _pointsRepository.getTransactions();
      yield TransactionsLoaded(transactions: newTransactionList);
    } catch (err) {
      yield TransactionsError();
    }
  }

  Stream<PointsState> _mapAppStartedToTransactionsState() async* {
    yield TransactionsLoading();
    yield* _getTransactions(transactions: []);
  }


  Stream<PointsState> _getBalances({Balance balance}) async* {
    try {
      Balance newBalance = await _pointsRepository.getBalances();
      yield BalanceLoaded(balance: newBalance);
    } catch (err) {
      yield BalanceError();
    }
  }

  Stream<PointsState> _mapAppStartedToBalanceState() async* {
    yield BalanceLoading();
    yield* _getBalances(balance: null);
  }

}

