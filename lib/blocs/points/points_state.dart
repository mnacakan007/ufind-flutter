part of 'points_bloc.dart';

abstract class PointsState extends Equatable {
  const PointsState();

  @override
  List<Object> get props => [];
}


class PointsEmpty extends PointsState {}

class LeaderBoardEmpty extends PointsState {}

class LeaderBoardLoading extends PointsState {}

class LeaderBoardLoaded extends PointsState {
  final LeaderBoard leaderBoard;

  const LeaderBoardLoaded({this.leaderBoard});

  @override
  List<Object> get props => [leaderBoard];

  @override
  String toString() => 'SingleSurveyLoaded { leaderBoard: $leaderBoard }';
}

class LeaderBoardError extends PointsState {}



class BalanceEmpty extends PointsState {}

class BalanceLoading extends PointsState {}

class BalanceLoaded extends PointsState {
  final Balance balance;

  const BalanceLoaded({this.balance});

  @override
  List<Object> get props => [balance];

  @override
  String toString() => 'BalanceLoaded { balance: $balance }';
}

class BalanceError extends PointsState {}



class OrdersEmpty extends PointsState {}

class OrdersLoading extends PointsState {}

class OrdersLoaded extends PointsState {
  final  List<Order> orders;

  const OrdersLoaded({this.orders});

  @override
  List<Object> get props => [orders];

  @override
  String toString() => 'OrdersLoaded { orders: $orders }';
}

class OrdersError extends PointsState {}



class TransactionsEmpty extends PointsState {}

class TransactionsLoading extends PointsState {}

class TransactionsLoaded extends PointsState {
  final List<Transaction> transactions;

  const TransactionsLoaded({this.transactions});

  @override
  List<Object> get props => [transactions];

  @override
  String toString() => 'TransactionsLoaded { transactions: $transactions }';
}

class TransactionsError extends PointsState {}