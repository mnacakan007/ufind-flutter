part of 'notifications_bloc.dart';

abstract class NotificationEvent extends Equatable {
  const NotificationEvent();

  @override
  List<Object> get props => [];
}

class NotificationStarted extends NotificationEvent {}

class RefreshNotification extends NotificationEvent {}

class LoadMoreNotification extends NotificationEvent {
  final List<NotificationModel> notifications;

  const LoadMoreNotification({this.notifications});

  @override
  List<Object> get props => [notifications];

  @override
  String toString() => 'LoadMoreNotification { notifications: $notifications }';
}
