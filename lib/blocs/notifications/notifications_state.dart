part of 'notifications_bloc.dart';

abstract class NotificationState extends Equatable {
  const NotificationState();

  @override
  List<Object> get props => [];
}

// Initial State
class NotificationEmpty extends NotificationState {}

class NotificationLoading extends NotificationState {}

class NotificationLoaded extends NotificationState {
  final List<NotificationModel> notifications;

  const NotificationLoaded({this.notifications});

  @override
  List<Object> get props => [notifications];

  @override
  String toString() => 'NotificationLoaded { notifications: $notifications}';
}

// API Request Error
class NotificationError extends NotificationState {}
