import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ufind_flutter/models/notification_model.dart';
import 'package:ufind_flutter/repositories/notification_repository.dart';


part 'notifications_event.dart';
part 'notifications_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final NotificationRepository _notificationRepository;

  NotificationBloc({@required NotificationRepository notificationRepository})
      : assert(notificationRepository != null),
        _notificationRepository = notificationRepository;

  @override
  NotificationState get initialState => NotificationEmpty();

  @override
  Stream<NotificationState> mapEventToState(NotificationEvent event) async* {
    if (event is NotificationStarted) {
      yield* _mapAppStartedToState();
    }
  }

  Stream<NotificationState> _getNotifications({List<NotificationModel> notifications}) async* {
    // Request offers
    try {
      List<NotificationModel> newNotificationList =
          notifications + await _notificationRepository.getNotifications();
      yield NotificationLoaded(notifications: newNotificationList);
    } catch (err) {
      yield NotificationError();
    }
  }

  Stream<NotificationState> _mapAppStartedToState() async* {
    yield NotificationLoading();
    yield* _getNotifications(notifications: []);
  }

}
