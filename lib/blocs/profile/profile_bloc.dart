import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ufind_flutter/models/customerInfo_model.dart';
import 'package:ufind_flutter/repositories/profile_repository.dart';


part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  final ProfileRepository _profileRepository;

  ProfileBloc({@required ProfileRepository profileRepository})
      : assert(profileRepository != null),
        _profileRepository = profileRepository;

  @override
  ProfileState get initialState => ProfileEmpty();

  @override
  Stream<ProfileState> mapEventToState(ProfileEvent event) async* {
    if (event is GetProfileStarted) {
      yield* _mapAppStartedToGetMeState();
    }

  }

  Stream<ProfileState> _getMe({Customer customer}) async* {
    try {
      Customer newCustomer = await _profileRepository.getMe();
      yield ProfileLoaded(customer: newCustomer);
    } catch (err) {
      yield ProfileError();
    }
  }

  Stream<ProfileState> _mapAppStartedToGetMeState() async* {
    yield ProfileLoading();
    yield* _getMe(customer: null);
  }


}



