part of 'profile_bloc.dart';

abstract class ProfileState extends Equatable {
  const ProfileState();

  @override
  List<Object> get props => [];
}

class ProfileEmpty extends ProfileState {}

class ProfileLoading extends ProfileState {}

class ProfileLoaded extends ProfileState {
  final Customer customer;

  const ProfileLoaded({this.customer});

  @override
  List<Object> get props => [customer];

  @override
  String toString() => 'ProfileLoaded { customer: $customer }';
}

class ProfileError extends ProfileState {}