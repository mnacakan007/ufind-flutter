part of 'registr_bloc.dart';

abstract class RegistrationState extends Equatable {
  const RegistrationState();

  @override
  List<Object> get props => [];
}

class RegistrationEmpty extends RegistrationState {}

class RegistrationLoading extends RegistrationState {}

class RegistrationLoaded extends RegistrationState {
  final Customer userData;

  const RegistrationLoaded({this.userData});

  @override
  List<Object> get props => [userData];

  @override
  String toString() => 'RegistrationLoaded { userData: $userData }';
}

class RegistrationError extends RegistrationState {}



class CustomerInfoEmpty extends RegistrationState {}

class CustomerInfoLoading extends RegistrationState {}

class CustomerInfoLoaded extends RegistrationState {
  final Facebook customerInfo;

  const CustomerInfoLoaded({this.customerInfo});

  @override
  List<Object> get props => [customerInfo];

  @override
  String toString() => 'CustomerInfoLoaded { customerInfo: $customerInfo }';
}

class CustomerInfoError extends RegistrationState {}



class CustomerMeEmpty extends RegistrationState {}

class CustomerMeLoading extends RegistrationState {}

class CustomerMeLoaded extends RegistrationState {
  final Customer customer;

  const CustomerMeLoaded({this.customer});

  @override
  List<Object> get props => [customer];

  @override
  String toString() => 'CustomerMeLoaded { customer: $customer }';
}

class CustomerMeError extends RegistrationState {}



class CustomerMeUpdateEmpty extends RegistrationState {}

class CustomerMeUpdateLoading extends RegistrationState {}

class CustomerMeUpdateLoaded extends RegistrationState {
  final Customer updateUserData;

  const CustomerMeUpdateLoaded({this.updateUserData});

  @override
  List<Object> get props => [updateUserData];

  @override
  String toString() => 'CustomerMeLoaded { updateUserData: $updateUserData }';
}

class CustomerMeUpdateError extends RegistrationState {}