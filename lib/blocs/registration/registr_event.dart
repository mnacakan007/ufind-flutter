part of 'registr_bloc.dart';

abstract class RegistrationEvent extends Equatable {
  const RegistrationEvent();

  @override
  List<Object> get props => [];
}

class GetMeStarted extends RegistrationEvent {}

class UpdateUser extends RegistrationEvent {
  final dynamic customer;

  const UpdateUser({this.customer});

  @override
  List<Object> get props => [customer];

  @override
  String toString() => 'UpdateUser { customer: $customer}';
}

class GetCustomerInfo extends RegistrationEvent {
  final String customerToken;
  final String userId;

  const GetCustomerInfo({this.customerToken, this.userId});

  @override
  List<Object> get props => [customerToken, userId];

  @override
  String toString() => 'GetCustomerInfo { customerToken: $customerToken, userId: $userId}';
}

class CreateUser extends RegistrationEvent {
  final dynamic customer;

  const CreateUser({this.customer});

  @override
  List<Object> get props => [customer];

  @override
  String toString() => 'CreateUser { customer: $customer}';
}
