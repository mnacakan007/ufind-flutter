import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ufind_flutter/models/customerInfo_model.dart';
import 'package:ufind_flutter/models/facebook_model.dart';
import 'package:ufind_flutter/repositories/customerInfo_repository.dart';

part 'registr_event.dart';
part 'registr_state.dart';

class RegistrationBloc extends Bloc<RegistrationEvent, RegistrationState> {
  final CustomerRepository _customerRepository;

  RegistrationBloc({@required CustomerRepository customerRepository})
      : assert(customerRepository != null),
        _customerRepository = customerRepository;

  @override
  RegistrationState get initialState => CustomerMeEmpty();

  @override
  Stream<RegistrationState> mapEventToState(RegistrationEvent event) async* {
    if (event is GetMeStarted) {
      yield* _mapAppStartedToGetMeState();
    } else if (event is GetCustomerInfo) {
      yield* _mapAppStartedToGetCustomerInfoState(event.customerToken, event.userId);
    } else if (event is CreateUser) {
      yield* _mapAppStartedToCreateUserState(event.customer);
    } else if (event is UpdateUser) {
      yield* _mapAppStartedToUpdateUserState(event.customer);
    }
  }

  Stream<RegistrationState> _getMe({Customer customer}) async* {
    try {
      Customer newCustomer = await _customerRepository.getMe();
      yield CustomerMeLoaded(customer: newCustomer);
    } catch (err) {
      yield CustomerMeError();
    }
  }

  Stream<RegistrationState> _mapAppStartedToGetMeState() async* {
    yield CustomerMeLoading();
    yield* _getMe(customer: null);
  }


  Stream<RegistrationState> _getCustomerInfo({Facebook customerInfo, String customerToken = '', String userId = ''}) async* {
    try {
      Facebook newCustomerInfo = await _customerRepository.getCustomerInfo(customerToken: customerToken, userId: userId);
      print('newCustomerInfo $newCustomerInfo');
      yield CustomerInfoLoaded(customerInfo: newCustomerInfo);
    } catch (err) {
      yield CustomerInfoError();
    }
  }

  Stream<RegistrationState> _mapAppStartedToGetCustomerInfoState(customerToken, userId) async* {
    yield CustomerInfoLoading();
    yield* _getCustomerInfo(customerInfo: null, customerToken: customerToken, userId: userId);
  }


  Stream<RegistrationState> _createUser({Customer userData, dynamic customer}) async* {
    try {
      Customer newUserData = await _customerRepository.createUser(customer: customer);
      print('newCustomerInfo $newUserData');
      yield RegistrationLoaded(userData: newUserData);
    } catch (err) {
      yield RegistrationError();
    }
  }

  Stream<RegistrationState> _mapAppStartedToCreateUserState(customer) async* {
    yield RegistrationLoading();
    yield* _createUser(userData: null, customer: customer);
  }



  Stream<RegistrationState> _updateUser({Customer updateUserData, dynamic customer}) async* {
    try {
      Customer newUpdateUserData = await _customerRepository.updateUser(customer: customer);
      print('newUpdateUserData $newUpdateUserData');
      yield CustomerMeUpdateLoaded(updateUserData: newUpdateUserData);
    } catch (err) {
      yield CustomerMeUpdateError();
    }
  }

  Stream<RegistrationState> _mapAppStartedToUpdateUserState(customer) async* {
    yield CustomerMeUpdateLoading();
    yield* _updateUser(updateUserData: null, customer: customer);
  }


}



