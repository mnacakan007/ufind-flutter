part of 'surveys_bloc.dart';

abstract class SurveyState extends Equatable {
  const SurveyState();

  @override
  List<Object> get props => [];
}

// Initial State
class SurveyEmpty extends SurveyState {}

class SurveyLoading extends SurveyState {}

class SurveyLoaded extends SurveyState {
  final List<Survey> surveys;

  const SurveyLoaded({this.surveys});

  @override
  List<Object> get props => [surveys];

  @override
  String toString() => 'SurveyLoaded { surveys: $surveys}';
}

// API Request Error
class SurveyError extends SurveyState {}
