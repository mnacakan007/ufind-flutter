part of 'surveys_bloc.dart';

abstract class SurveyEvent extends Equatable {
  const SurveyEvent();

  @override
  List<Object> get props => [];
}

class SurveyStarted extends SurveyEvent {}

class RefreshSurveys extends SurveyEvent {}

class LoadMoreSurveys extends SurveyEvent {
  final List<Survey> surveys;

  const LoadMoreSurveys({this.surveys});

  @override
  List<Object> get props => [surveys];

  @override
  String toString() => 'LoadMoreSurveys { surveys: $surveys }';
}
