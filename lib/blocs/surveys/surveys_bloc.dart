import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:ufind_flutter/models/survey_model.dart';
import 'package:ufind_flutter/repositories/surveys_repository.dart';

part 'surveys_event.dart';
part 'surveys_state.dart';

class SurveyBloc extends Bloc<SurveyEvent, SurveyState> {
  final SurveyRepository _surveyRepository;

  SurveyBloc({@required SurveyRepository surveyRepository})
      : assert(surveyRepository != null),
        _surveyRepository = surveyRepository;

  @override
  SurveyState get initialState => SurveyEmpty();

  @override
  Stream<SurveyState> mapEventToState(SurveyEvent event) async* {
    if (event is SurveyStarted) {
      yield* _mapAppStartedToState();
    } else if (event is RefreshSurveys) {
      yield* _getSurveys(surveys: []);
    } else if (event is LoadMoreSurveys) {
      yield* _mapLoadMoreSurveysToState(event);
    }
  }

  Stream<SurveyState> _getSurveys({List<Survey> surveys, int page = 0}) async* {
    // Request surveys
    try {
      List<Survey> newSurveysList =
          surveys + await _surveyRepository.getSurveys(page: page);
      yield SurveyLoaded(surveys: newSurveysList);
    } catch (err) {
      yield SurveyError();
    }
  }

  Stream<SurveyState> _mapAppStartedToState() async* {
    yield SurveyLoading();
    yield* _getSurveys(surveys: []);
  }

  Stream<SurveyState> _mapLoadMoreSurveysToState(LoadMoreSurveys event) async* {
    final int nextPage = event.surveys.length ~/ SurveyRepository.perPage;
    yield* _getSurveys(surveys: event.surveys, page: nextPage);
  }
}
