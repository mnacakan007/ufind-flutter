import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/registration/registr_bloc.dart';
import 'package:ufind_flutter/models/facebook_model.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';

enum GenderList { male, female }

// ignore: must_be_immutable
class RegistrationScreen extends StatefulWidget {
  CustomerInfoLoaded customerInfoLoaded;

  RegistrationScreen(CustomerInfoLoaded customerInfoLoaded) {
    this.customerInfoLoaded = customerInfoLoaded;
  }

  @override
  _RegistrationScreenState createState() => _RegistrationScreenState();
}


class _RegistrationScreenState extends State<RegistrationScreen> {
  final _formKey = GlobalKey<FormState>();
  GenderList _gender;
  bool _agreement = false;
  Facebook customerInfo;
  String _format = 'MMMM-dd-yyyy';
  var format = DateFormat('MMMM-dd-yyyy');
  TextEditingController _formatCtrl = TextEditingController();
  DateTime _dateTime;
  DateTime _minDatetime;
  String time = DateTime.now().toIso8601String();
  Model model = Model();

  @override
  void initState() {
    super.initState();
    _formatCtrl.text = _format;
    String min = '${int.parse(time.split('-')[0]) - 12}-${time.split(
        '-')[1]}-${time.split('-')[2]}';
    _minDatetime = _dateTime = DateTime.parse(min);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
            child: Text('Գրանցում',
                style: TextStyle(fontWeight: FontWeight.bold))),
      ),
      body: Stack(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(10.0),
              height: double.infinity,
              child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Օգտատեր:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      TextFormField(
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Օգտատիրոջ անունը պարտադիր է';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          model.username = value;
                        },
                      ),

                      SizedBox(height: 10.0),

                      Text(
                        'Անուն:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Անունը պարտադիր է';
                            }
                            return null;
                          },
                          initialValue: widget.customerInfoLoaded.customerInfo?.firstName,
                          onSaved: (String value) {
                            model.firstName = value;
                          }
                      ),

                      SizedBox(height: 10.0),

                      Text(
                        'Ազգանուն:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Անունը պարտադիր է';
                            }
                            return null;
                          },
                          initialValue: widget.customerInfoLoaded.customerInfo?.lastName,
                          onSaved: (String value) {
                            model.lastName = value;
                          }
                      ),

                      SizedBox(height: 10.0),

                      Text(
                        'Ծննդյան ամսաթիվ:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          RaisedButton(
                            onPressed: _showDatePicker,
                            color: Colors.green,
                            child: Icon(Icons.date_range, color: Colors.white,),
                          ),
                          Text(
                            '${_dateTime.year}-${_dateTime.month.toString()
                                .padLeft(2, '0')}-${_dateTime.day.toString()
                                .padLeft(2, '0')}',
                            style: Theme
                                .of(context)
                                .textTheme
                                .headline6,
                          ),
                        ],
                      ),

                      SizedBox(height: 10.0),

                      Text(
                        'Սեռ:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      RadioListTile(
                        title: const Text('Արական'),
                        value: GenderList.male,
                        groupValue: _gender,
                        onChanged: (GenderList value) {
                          setState(() {
                            _gender = value;
                          });
                        },
                      ),

                      RadioListTile(
                        title: const Text('Իգական'),
                        value: GenderList.female,
                        groupValue: _gender,
                        onChanged: (GenderList value) {
                          setState(() {
                            _gender = value;
                          });
                        },
                      ),

                      SizedBox(height: 5.0),

                      CheckboxListTile(
                          value: _agreement,
                          title: Text(
                              'Ես ընդունում եմ պայմաններն ու գաղտնիության քաղաքականությունը',
                              style: TextStyle(fontSize: 14)),
                          onChanged: (bool value) =>
                              setState(() => _agreement = value)),

                      SizedBox(height: 50.0),

                    ],
                  ))),
          Positioned(
            bottom: 15.0,
            left: 15.0,
            right: 15.0,
            child: RaisedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  Color color = Colors.red;
                  String text;
                  print('model: ${this.model.username}');
                  if (_gender == null)
                    text = 'Սեռն անհրաժեշտ է';
                  else if (_agreement == false)
                    text = 'Դուք պետք է ընդունեք համաձայնագրի պայմանները';
                  else if (_dateTime == _minDatetime) {
                    text = 'Ծննդյան ամսաթիվը պարտադիր է';
                  }
                  else {
                    dynamic customer = {
                      'username': '${this.model?.username}',
                      'firstName': '${this.model?.firstName}',
                      'lastName': '${this.model?.lastName}',
                      'gender': '$_gender'.split('.')[1],
                      'dateOfBirth': '$_dateTime'.split(' ')[0],
                      'phoneNumber': '',
                      'photoUrl': '${widget.customerInfoLoaded.customerInfo?.picture}',
                      'refererID': null,
                      'facebookProfile': {},
                      'termsAndConditions': _agreement
                    };

                    print('customer: $customer');
                    text = 'Форма успешно заполнена';
                    color = Colors.green;
                    context.bloc<RegistrationBloc>().add(CreateUser(customer: customer));
                    checkRegistrationStatus();
                  }

                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text(text),
                    backgroundColor: color,
                  ));
                }
              },
              child: Text('Գրանցվել'),
              color: Colors.blue,
              textColor: Colors.white,
            ),
          ),
        ],
      ),
    );
  }

  /// Display date picker.
  void _showDatePicker() {
    DatePicker.showDatePicker(
      context,
      onMonthChangeStartWithFirstDate: true,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text('Done', style: TextStyle(color: Colors.green)),
      ),
      maxDateTime: _minDatetime,
      initialDateTime: _dateTime,
      dateFormat: _format,
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
        });
      },
    );
  }

  checkRegistrationStatus() {
    return BlocBuilder<RegistrationBloc, RegistrationState>(
      builder: (context, state) {
        return Container(
          child: _buildBody(state),
        );
      },
    );
  }

  _buildBody(RegistrationState state) {
    print('RegistrationState $state');
    if (state is RegistrationLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is RegistrationLoaded) {
      print('userData ${state.userData}');

    } else if (state is RegistrationError) {
      return Center(
        child: Text(
          'Error loading registration!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

}

class Model {
  String username;
  String firstName;
  String lastName;
  String gender;
  dynamic dateOfBirth;

  Model({this.username, this.firstName, this.lastName, this.gender, this.dateOfBirth});
}