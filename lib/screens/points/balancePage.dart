import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ufind_flutter/blocs/points/points_bloc.dart';
import 'package:ufind_flutter/screens/points/transactionPage.dart';
import 'package:ufind_flutter/screens/points/ordersPage.dart';

// ignore: must_be_immutable
class BalancePage extends StatefulWidget {
  BalanceLoaded balanceState;

  BalancePage(BalanceLoaded balanceState) {
    this.balanceState = balanceState;
  }

  @override
  _BalancePageState createState() => _BalancePageState();
}

class _BalancePageState extends State<BalancePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Scaffold(
          appBar: AppBar(
              bottom: PreferredSize(
                preferredSize: Size.fromHeight(110.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(bottom: 40.0, left: 20.0, right: 20.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('Դուք ունեք', style: TextStyle(fontSize: 13.0)),
                          Row(
                            children: <Widget>[
                              Text('U ${widget.balanceState.balance?.amount ?? 0 }',
                                  style: TextStyle(
                                      fontSize: 25.0, fontWeight: FontWeight.w600)),
                              Text(' միավոր', style: TextStyle(fontSize: 25.0)),
                            ],
                          ),
                        ],
                      ),
                    ),
                    TabBar(
                      indicatorColor: Colors.lightBlue,
                      tabs: <Widget>[
                        Tab(
                          text: "Գնված առաջարկներ".toUpperCase(),
                        ),
                        Tab(
                          text: "Գործարքներ".toUpperCase(),
                        )
                      ],
                    ),
                  ],
                ),
              )),
          body: TabBarView(
            children: <Widget>[OrdersPage(), TransactionPage()],
          )),
    ));
  }
}
