import 'dart:convert';
import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/points_model.dart';

// ignore: must_be_immutable
class LeaderBoardByIdPage extends StatefulWidget {
  @override
  _LeaderBoardByIdPageState createState() => _LeaderBoardByIdPageState();
}

class _LeaderBoardByIdPageState extends State<LeaderBoardByIdPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  LeaderBoard leaderBoardState;
  List<LeaderBoardUser> leaderBoardUser = [];
  dynamic leader;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLeaderboard();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Առաջատարների ցանկ', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
        ),
        body: leaderBoardState != null
            ? Stack(
                children: <Widget>[
                  Container(
                    height: double.infinity,
                    padding: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 0.0),
                    margin: EdgeInsets.only(top: 20),
                    child: SingleChildScrollView(
                      physics: AlwaysScrollableScrollPhysics(),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: double.infinity,
                            height: 250.0,
                            child: Stack(
                              children: <Widget>[
                                ClipRRect(
                                  borderRadius: BorderRadius.only(
                                      topLeft: const Radius.circular(8.0),
                                      topRight: const Radius.circular(8.0)),
                                  child: Image.asset(
                                    'assets/fon/leaderboard.png',
                                    height: double.infinity,
                                    width: double.infinity,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Positioned.fill(
                                  top: 20.0,
                                  right: 10.0,
                                  child: Align(
                                    alignment: Alignment.topCenter,
                                    child: Text('${leaderBoardState.place}',
                                        style: TextStyle(
                                            fontSize: 44.0,
                                            fontWeight: FontWeight.bold)),
                                  ),
                                ),
                                Positioned.fill(
                                  top: 70.0,
                                  right: 10.0,
                                  child: Align(
                                    alignment: Alignment.topCenter,
                                    child: Text('տեղ',
                                        style: TextStyle(fontSize: 16.0)),
                                  ),
                                ),
                                Positioned.fill(
                                  top: 90.0,
                                  right: 10.0,
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: Text('${leaderBoardState.score}',
                                        style: TextStyle(
                                            fontSize: 50.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white)),
                                  ),
                                ),
                                Positioned.fill(
                                  bottom: 30.0,
                                  right: 10.0,
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Text('ՄԻԱՎՈՐ',
                                        style: TextStyle(
                                            fontSize: 18.0,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.white)),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Container(
                              width: double.infinity,
                              height: MediaQuery.of(context).size.height * 0.5,
                              child: Stack(
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.all(5.0),
                                    child: ListView.builder(
                                      itemCount: leaderBoardUser.length,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        leader = leaderBoardUser[index];

                                        return leaderBoardUser.length != 0
                                            ? Container(
                                                padding: EdgeInsets.fromLTRB(
                                                    5.0, 5.0, 5.0, 5.0),
                                                child: Stack(
                                                  children: <Widget>[
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .center,
                                                      children: <Widget>[
                                                        Row(
                                                          children: <Widget>[
                                                            Stack(
                                                              children: <
                                                                  Widget>[
                                                                Container(
                                                                  width: 50.0,
                                                                  height: 50.0,
                                                                  decoration:
                                                                      new BoxDecoration(
                                                                    gradient:
                                                                        LinearGradient(
                                                                      colors: [
                                                                        Color.fromRGBO(
                                                                            72,
                                                                            37,
                                                                            8,
                                                                            1.0),
                                                                        Color.fromRGBO(
                                                                            178,
                                                                            79,
                                                                            150,
                                                                            1.0)
                                                                      ],
                                                                    ),
                                                                    shape: BoxShape
                                                                        .circle,
                                                                  ),
                                                                ),
                                                                Positioned(
                                                                  left: 19.0,
                                                                  top: 10.0,
                                                                  child: Text(
                                                                    '${leaderBoardUser[index].place}',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            20.0),
                                                                  ),
                                                                )
                                                              ],
                                                            ),
                                                            SizedBox(
                                                                width: 5.0),
                                                            Text(
                                                                leader.name
                                                                    .toUpperCase(),
                                                                style: TextStyle(
                                                                    fontSize:
                                                                        24.0)),
                                                          ],
                                                        ),
                                                        Text('${leader.score}',
                                                            style: TextStyle(
                                                                fontSize: 28.0,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: Colors
                                                                    .pink)),
                                                      ],
                                                    )
                                                  ],
                                                ))
                                            : Container();
                                      },
                                    ),
                                  ),
                                ],
                              ))
                        ],
                      ),
                    ),
                  ),
                ],
              )
            : Container());
  }

  Future<LeaderBoard> getLeaderboard() async {
    final SharedPreferences prefs = await _prefs;
    dynamic leaderBoardMap = json.decode(prefs.getString('leaderBoard'));

    setState(() {
      leaderBoardState = LeaderBoard.fromJson(leaderBoardMap);
      leaderBoardState.topUsers.forEach(
          (json) => leaderBoardUser.add(LeaderBoardUser.fromJson(json)));
    });

    return leaderBoardState;
  }
}
