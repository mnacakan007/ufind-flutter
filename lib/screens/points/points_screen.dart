import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/points/points_bloc.dart';
import 'package:ufind_flutter/screens/points/balancePage.dart';
import 'leaderBoardPage.dart';

class PointsScreen extends StatefulWidget {
  @override
  _PointsScreenState createState() => _PointsScreenState();
}

class _PointsScreenState extends State<PointsScreen> {
  @override
  void initState() {
    super.initState();
    context.bloc<PointsBloc>().add(PointsStarted());
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Scaffold(
          appBar: AppBar(
            title: TabBar(
              indicatorColor: Colors.lightBlue,
              tabs: <Widget>[
                Tab(
                  text: "Հարցումներ".toUpperCase(),
                ),
                Tab(
                  text: "Մրցենք".toUpperCase(),
                )
              ],
            ),
          ),
          body: BlocBuilder<PointsBloc, PointsState>(
            builder: (context, state) {
              return Container(
                child: _buildBody(state),
              );
            },
          ),
        ));
  }

  _buildBody(PointsState state) {
    if (state is BalanceLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is BalanceLoaded) {
      return TabBarView(
        children: <Widget>[BalancePage(state), LeaderBoardPage()],
      );
    } else if (state is BalanceError) {
      return Center(
        child: Text(
          'Error loading balance!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

}




