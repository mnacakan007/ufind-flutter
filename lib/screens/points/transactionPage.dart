import 'dart:convert';
import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/points_model.dart';

// ignore: must_be_immutable
class TransactionPage extends StatefulWidget {
  @override
  _TransactionPageState createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  List<Transaction> transactions = [];
  var format = DateFormat.yMMMd().add_jms();
  dynamic transaction;

  @override
  void initState() {
    super.initState();
    getTransactions();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Text('Գործարքներ',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
          ),
          transactions.length != 0
              ? Padding(
                  padding: EdgeInsets.only(
                      top: 60.0, left: 20.0, right: 20.0, bottom: 20.0),
                  child: ListView.builder(
                    itemCount: transactions.length,
                    itemBuilder: (BuildContext context, int index) {
                      transaction = transactions[index];

                      return Container(
                        padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 20.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      alignment: AlignmentDirectional(0.0, 0.0),
                                      child: Container(
                                        child: Text('${transaction.details}',
                                            style: TextStyle(fontSize: 20.0)),
                                        constraints: BoxConstraints(
                                            maxHeight: 150.0,
                                            maxWidth: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.44,
                                            minWidth: 100.0,
                                            minHeight: 20.0),
                                      ),
                                    ),
                                    Text(
                                        transaction.amount >= 0
                                            ? 'U${transaction.amount}.00'
                                            : '-U${(transaction.amount).abs()}',
                                        style: TextStyle(
                                            fontSize: 20.0,
                                            color: transaction.amount >= 0
                                                ? Colors.lightGreen
                                                : Colors.red)),
                                  ],
                                ),
                              ],
                            ),
                            Container(
                              alignment: AlignmentDirectional(0.0, 0.0),
                              child: Container(
                                child: Text(
                                    '${format.format(DateTime.fromMillisecondsSinceEpoch(transaction.createdAt * 1000))}',
                                    style: TextStyle(
                                        fontSize: 13.0, color: Colors.black38)),
                                constraints: BoxConstraints(
                                    maxHeight: 150.0,
                                    maxWidth: 160.0,
                                    minWidth: 100.0,
                                    minHeight: 40.0),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                )
              : Center(
                  child: Container(
                      padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
                      child: Text(
                        'Այս պահին գործարքներ չունեք',
                      )),
                ),
        ],
      ),
    );
  }

  getTransactions() async {
    final SharedPreferences prefs = await _prefs;
    List<dynamic> transactionsList =
        json.decode(prefs.getString('transactions'));
    setState(() {
      if (transactionsList != null) {
        transactionsList.forEach(
          (json) => transactions.add(Transaction.fromJson(json)),
        );
      }
    });
  }
}
