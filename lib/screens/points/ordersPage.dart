import 'dart:convert';
import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:ufind_flutter/models/points_model.dart';

// ignore: must_be_immutable
class OrdersPage extends StatefulWidget {
  @override
  _OrdersPageState createState() => _OrdersPageState();
}

class _OrdersPageState extends State<OrdersPage> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  List<Order> ordersState = [];
  var format = DateFormat.yMMMd();
  dynamic order;

  @override
  void initState() {
    super.initState();
    getOrders();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(20.0),
            child: Text('Գնված առաջարկներ',
                style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold)),
          ),
          ordersState.length != 0
              ? Padding(
                  padding: EdgeInsets.only(
                      top: 60.0, left: 20.0, right: 20.0, bottom: 20.0),
                  child: ListView.builder(
                    itemCount: ordersState.length,
                    itemBuilder: (BuildContext context, int index) {
                      order = ordersState[index];

                      return Container(
                        padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Image.asset(
                                  'assets/QR_code.png',
                                  height: 35.0,
                                  width: 35.0,
                                  fit: BoxFit.cover,
                                ),
                                SizedBox(width: 15.0),
                                Column(
                                  children: <Widget>[
                                    Text('Զեղչ',
                                        style: TextStyle(fontSize: 20.0)),
                                    Text('${order.discount}%',
                                        style: TextStyle(fontSize: 20.0)),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Text('Վավեր է մինչև',
                                    style: TextStyle(fontSize: 12.0)),
                                SizedBox(width: 5.0),
                                Text(
                                    '${format.format(DateTime.fromMillisecondsSinceEpoch(order.availableDates[1] * 1000))}',
                                    style: TextStyle(fontSize: 12.0))
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                )
              : Center(
                  child: Text(
                    'Այս պահին գնված առաջարկներ չունեք',
                  ),
                )
        ],
      ),
    );
  }

  getOrders() async {
    final SharedPreferences prefs = await _prefs;
    List<dynamic> orderList = json.decode(prefs.getString('orders'));
    setState(() {
      if (orderList != null) {
        orderList.forEach(
          (json) => ordersState.add(Order.fromJson(json)),
        );
      }
    });
  }
}
