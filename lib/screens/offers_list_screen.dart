import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/offer/single_offer_bloc.dart';
import 'package:ufind_flutter/blocs/offers/offers_bloc.dart';
import 'package:ufind_flutter/screens/single_offer_screen.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class OffersListScreen extends StatefulWidget {
  OfferLoaded state;

  OffersListScreen(OfferLoaded state) {
    this.state = state;
  }

  @override
  _OffersListScreenState createState() => _OffersListScreenState();
}

class _OffersListScreenState extends State<OffersListScreen> {
  dynamic offer = [];
  String subject = 'Պատասխանիր և ստացիր U միավորներ';
  var format = DateFormat.yMMMd();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: widget.state.offers.length != 0
          ? ListView.builder(
        itemCount: widget.state.offers.length,
        itemBuilder: (BuildContext context, int index) {
          offer = widget.state.offers[index];

          return Container(
                  padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                  margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: double.infinity,
                        height: 200.0,
                        constraints: BoxConstraints.tightFor(width: 100.0),
                        child: Stack(
                          children: <Widget>[
                            ClipRRect(
                              borderRadius: BorderRadius.only(
                                  topLeft: const Radius.circular(8.0),
                                  topRight: const Radius.circular(8.0)),
                              child: Image.network(
                                offer.featuredImageUrl,
                                fit: BoxFit.cover,
                                width: double.infinity,
                              ),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 15.0, left: 15.0),
                                  child: Container(
                                    width: 110.0,
                                    height: 30.0,
                                    child: RaisedButton(
                                      onPressed: offer.title.isEmpty
                                          ? null
                                          : () {
                                              final RenderBox box =
                                                  context.findRenderObject();
                                              Share.share(offer.title,
                                                  subject: subject,
                                                  sharePositionOrigin:
                                                      box.localToGlobal(
                                                              Offset.zero) &
                                                          box.size);
                                            },
                                      color: Colors.cyan,
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5)),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Text(
                                            'Կիսվել'.toUpperCase(),
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 12.0),
                                          ),
                                          SizedBox(
                                            width: 3.0,
                                          ),
                                          Icon(
                                            Icons.share,
                                            color: Colors.white,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      top: 15.0, right: 15.0),
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "${offer.discount}%",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 32.0,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Text(
                                        offer.discountType,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14.0),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              offer.partnerName.toUpperCase(),
                              style: TextStyle(
                                  fontSize: 13.0, fontWeight: FontWeight.bold),
                            ),
                            Text(
                              offer.title,
                              style: TextStyle(
                                  fontSize: 24.0, fontWeight: FontWeight.bold),
                            ),
                            SizedBox(height: 10.0),
                            Text(
                              widget.state.offers[index].description.length > 80
                                  ? '${widget.state.offers[index].description.substring(0, 80)}...'
                                  : widget.state.offers[index].description,
                              style: TextStyle(fontSize: 16.0),
                            ),
                            SizedBox(height: 25.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Text('Առկա է  ${offer.amount - offer.claimed}',
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold)),
                                Text(
                                    '${format.format(DateTime.fromMillisecondsSinceEpoch(offer.availableDates[0] * 1000))} - '
                                    '${format.format(DateTime.fromMillisecondsSinceEpoch(offer.availableDates[1] * 1000))}',
                                    style: TextStyle(fontSize: 17.0))
                              ],
                            ),
                            SizedBox(height: 15.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  child: Row(children: <Widget>[
                                    Icon(
                                      Icons.account_balance_wallet,
                                      color: Colors.amber,
                                    ),
                                    SizedBox(
                                      width: 5.0,
                                    ),
                                  ]),
                                ),
                                SizedBox(width: 15.0),
                                Text('U ${offer.price} միավոր',
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold)),
                              ],
                            )
                          ],
                        ),
                      ),
                      RaisedButton(
                        onPressed: () {
                          context.bloc<SingleOfferBloc>().add(SingleOfferStarted(offerId: widget.state.offers[index].id));
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    SingleOfferScreen(widget.state.offers[index].title)),
                          );
                        },
                        color: Colors.blueAccent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        child: Container(
                          width: double.infinity,
                          height: 50.0,
                          child: Center(
                            child: Text(
                              'Իմանալ ավելին',
                              style: TextStyle(
                                  color: Colors.white, fontSize: 16.0),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                );
        },
      ) : Center(
        child: Container(
            padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
            child: Text(
              'Այս պահին նոր առաջարկներ չունեք',
              style: TextStyle(fontSize: 16.0),
            )),
      ),
    );
  }
}
