import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cupertino_date_picker/flutter_cupertino_date_picker.dart';
import 'package:intl/intl.dart';
import 'package:kf_drawer/kf_drawer.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/profile/profile_bloc.dart';
import 'package:ufind_flutter/blocs/registration/registr_bloc.dart';
import 'package:ufind_flutter/models/facebook_model.dart';
import 'package:ufind_flutter/nav/drawer_menu.dart';
import 'package:ufind_flutter/screens/registration_screen.dart';

enum GenderList {male, female}


// ignore: must_be_immutable
class ProfileScreen extends KFDrawerContent {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  final _formKey = GlobalKey<FormState>();
  GenderList _gender;
  bool _agreement = false;
  Facebook customerInfo;
  String _format = 'MMMM-dd-yyyy';
  var format = DateFormat('MMMM-dd-yyyy');
  TextEditingController _formatCtrl = TextEditingController();
  DateTime _dateTime;
  DateTime _maxDatetime;
  String time = DateTime.now().toIso8601String();
  Model model = Model();

  @override
  void initState() {
    super.initState();

    context.bloc<ProfileBloc>().add(GetProfileStarted());

    _formatCtrl.text = _format;
    String max = '${int.parse(time.split('-')[0]) - 12}-${time.split(
        '-')[1]}-${time.split('-')[2]}';
    _maxDatetime = DateTime.parse(max);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Անձնական տվյալներ',
                  style: TextStyle(fontWeight: FontWeight.bold)),
          leading: ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(32.0)),
            child: Material(
              shadowColor: Colors.transparent,
              color: Colors.transparent,
              child: IconButton(
                icon: Icon(
                  Icons.menu,
                  color: Colors.black,
                ),
                onPressed: widget.onMenuPressed,
              ),
            ),
          ),
        ),
        body: BlocBuilder<ProfileBloc, ProfileState>(
          builder: (context, state) {
            return Container(
              child: _buildBody(state),
            );
          },
        ),
      ),
    );
  }
  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child: Text("NO"),
          ),
          SizedBox(height: 16),
          GestureDetector(
            onTap: () => Navigator.of(context).pop(true),
            child: Text("YES"),
          ),
        ],
      ),
    ) ??
        false;
  }


  _buildBody(ProfileState state) {
    if (state is ProfileLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is ProfileLoaded) {

      if (state.customer?.gender == 'male') {
        _gender = GenderList?.male;
      } else {
        _gender = GenderList?.female;
      }

      return Stack(
        children: <Widget>[
          Container(
              padding: EdgeInsets.all(20.0),
              height: double.infinity,
              child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Օգտատեր:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      TextFormField(
                        validator: (String value) {
                          if (value.isEmpty) {
                            return 'Օգտատիրոջ անունը պարտադիր է';
                          }
                          return null;
                        },
                        onSaved: (String value) {
                          model.username = value;
                        },
                        initialValue: state.customer?.username,
                      ),

                      SizedBox(height: 10.0),

                      Text(
                        'Անուն:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Անունը պարտադիր է';
                            }
                            return null;
                          },
                          initialValue: state.customer?.firstName,
                          onSaved: (String value) {
                            model.firstName = value;
                          }
                      ),

                      SizedBox(height: 10.0),

                      Text(
                        'Ազգանուն:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      TextFormField(
                          validator: (String value) {
                            if (value.isEmpty) {
                              return 'Անունը պարտադիր է';
                            }
                            return null;
                          },
                          initialValue: state.customer?.lastName,
                          onSaved: (String value) {
                            model.lastName = value;
                          }
                      ),

                      SizedBox(height: 10.0),

                      Text(
                        'Ծննդյան ամսաթիվ:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          RaisedButton(
                            onPressed: _showDatePicker,
                            color: Colors.green,
                            child: Icon(Icons.date_range, color: Colors.white),
                          ),
                          _dateTime == null ? changeDateTime(DateTime.parse(state.customer?.dateOfBirth)) : changeDateTime(_dateTime)
                        ],
                      ),

                      SizedBox(height: 10.0),

                      Text(
                        'Սեռ:',
                        style: TextStyle(fontSize: 16.0),
                      ),

                      RadioListTile(
                        title: const Text('Արական'),
                        value: GenderList.male,
                        groupValue: _gender,
                        onChanged: (GenderList value) {
                          setState(() {
                            _gender = value;
                          });
                        },
                      ),

                      RadioListTile(
                        title: const Text('Իգական'),
                        value: GenderList.female,
                        groupValue: _gender,
                        onChanged: (GenderList value) {
                          setState(() {
                            _gender = value;
                          });
                        },
                      ),

                      SizedBox(height: 50.0),

                    ],
                  ))),
          Positioned(
            bottom: 15.0,
            left: 15.0,
            right: 15.0,
            child: RaisedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  _formKey.currentState.save();
                  Color color = Colors.red;
                  String text;
                  print('model: ${this.model.username}');
                  if (_gender == null)
                    text = 'Սեռն անհրաժեշտ է';
                  else if (_dateTime == _maxDatetime) {
                    text = 'Ծննդյան ամսաթիվը պարտադիր է';
                  }
                  else {
                    dynamic customer = {
                      'username': '${this.model.username}',
                      'firstName': '${this.model.firstName}',
                      'lastName': '${this.model.lastName}',
                      'gender': '$_gender'.split('.')[1],
                      'dateOfBirth': '$_dateTime'.split(' ')[0],
                      'phoneNumber': '${state.customer.phoneNumber}',
                      'photoUrl': '${state.customer.photoUrl}',
                      'id': '${state.customer.id}',
                      'refererID': '${state.customer.refererID}',
                      'emailAddress': '${state.customer.emailAddress}',
                      'facebookProfile': {},
                      'termsAndConditions': _agreement
                    };

                    print('customer: $customer');
                    context.bloc<RegistrationBloc>().add(UpdateUser(customer: customer));
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MainWidget()),
                    );
                    text = 'Форма успешно заполнена';
                    color = Colors.green;
                  }

                  Scaffold.of(context).showSnackBar(SnackBar(
                    content: Text(text),
                    backgroundColor: color,
                  ));
                }
              },
              child: Text('Պահպանել'),
              color: Colors.blue,
              textColor: Colors.white,
            ),
          ),
        ],
      );
    } else if (state is ProfileError) {
      return Center(
        child: Text(
          'Error loading CustomerMeError!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  /// Display date picker.
  _showDatePicker() {
    DatePicker.showDatePicker(
      context,
      onMonthChangeStartWithFirstDate: true,
      pickerTheme: DateTimePickerTheme(
        showTitle: true,
        confirm: Text('Done', style: TextStyle(color: Colors.green)),
      ),
      maxDateTime: _maxDatetime,
      initialDateTime: _dateTime,
      dateFormat: _format,
      onClose: () => print("----- onClose -----"),
      onCancel: () => print('onCancel'),
      onChange: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
        });
      },
      onConfirm: (dateTime, List<int> index) {
        setState(() {
          _dateTime = dateTime;
        });
      },
    );
  }

  changeDateTime(dateTime) {
    return Text(
      '${dateTime.year}-${dateTime.month.toString()
          .padLeft(2, '0')}-${dateTime.day.toString()
          .padLeft(2, '0')}',
      style: Theme
          .of(context)
          .textTheme
          .headline6,
    );
  }

}
