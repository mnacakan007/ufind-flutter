import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:share/share.dart';
import 'package:ufind_flutter/blocs/offer/single_offer_bloc.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class SingleOfferScreen extends StatefulWidget {
  String title;

  SingleOfferScreen(String title) {
    this.title = title;
  }

  @override
  _SingleOfferScreenState createState() => _SingleOfferScreenState();
}

class _SingleOfferScreenState extends State<SingleOfferScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  String subject = 'Պատասխանիր և ստացիր U միավորներ';
  var format = DateFormat.yMMMd();
  dynamic offer = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        key: _scaffoldKey,
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: BlocBuilder<SingleOfferBloc, SingleOfferState>(
              builder: (context, state) {
                return Container(
                  child: _buildBody(state),
                );
              },
            ),
          ),
        )
    );
  }

  _buildBody(SingleOfferState state) {
    if (state is SingleOfferLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is SingleOfferLoaded) {
      dynamic offer = state.offer;
      return Stack(
        children: <Widget>[
          Container(
            height: double.infinity,
            padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
            margin: EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    width: double.infinity,
                    height: 200.0,
                    constraints: BoxConstraints.tightFor(width: 100.0),
                    child: Stack(
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.only(
                              topLeft: const Radius.circular(8.0),
                              topRight: const Radius.circular(8.0)),
                          child: Image.network(
                            offer.featuredImageUrl,
                            fit: BoxFit.cover,
                            width: double.infinity,
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 15.0, left: 15.0),
                              child: Container(
                                width: 110.0,
                                height: 30.0,
                                child: RaisedButton(
                                  onPressed: offer.title.isEmpty
                                      ? null
                                      : () {
                                    final RenderBox box =
                                    context.findRenderObject();
                                    Share.share(offer.title,
                                        subject: subject,
                                        sharePositionOrigin:
                                        box.localToGlobal(
                                            Offset.zero) &
                                        box.size);
                                  },
                                  color: Colors.cyan,
                                  shape: RoundedRectangleBorder(
                                      borderRadius:
                                      BorderRadius.circular(5)),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        'Կիսվել'.toUpperCase(),
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 12.0),
                                      ),
                                      SizedBox(
                                        width: 3.0,
                                      ),
                                      Icon(
                                        Icons.share,
                                        color: Colors.white,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 15.0, right: 15.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    "${offer.discount}%",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 32.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    offer.discountType,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 14.0),
                                  ),
                                ],
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          offer.partnerName.toUpperCase(),
                          style: TextStyle(
                              fontSize: 13.0, fontWeight: FontWeight.bold),
                        ),
                        Text(
                          offer.title,
                          style: TextStyle(
                              fontSize: 24.0, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          offer.description,
                          style: TextStyle(fontSize: 16.0),
                        ),
                        SizedBox(height: 25.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text('Առկա է  ${offer.amount - offer.claimed}',
                                style: TextStyle(
                                    fontSize: 18.0,
                                    fontWeight: FontWeight.bold)),
                            Text(
                                '${format.format(DateTime.fromMillisecondsSinceEpoch(offer.availableDates[0] * 1000))} - '
                                    '${format.format(DateTime.fromMillisecondsSinceEpoch(offer.availableDates[1] * 1000))}',
                                style: TextStyle(
                                    fontSize: 17.0))
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
              margin: EdgeInsets.all(10.0),
              child: RaisedButton(
                onPressed: () {
                  showAlertDialog(context);
                },
                color: Colors.blueAccent,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                child: Container(
                  width: double.infinity,
                  height: 50.0,
                  child: Center(
                    child: Text(
                      'Գնել U ${offer.price}',
                      style:
                      TextStyle(color: Colors.white, fontSize: 16.0),
                    ),
                  ),
                ),
              ),
            ),
          )
        ],
      );
    } else if (state is SingleOfferError) {
      return Center(
        child: Text(
          'Error loading offer!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  showAlertDialog(BuildContext context) {
    // set up the list options
    Widget optionOne = SimpleDialogOption(
      child: Text('Չեղարկել', style: TextStyle(color: Colors.red)),
      onPressed: () {
        setState(() {

        });
        Navigator.of(context).pop();
      },
    );
    Widget optionTwo = SimpleDialogOption(
      child: Text('Գնել', style: TextStyle(color: Colors.green)),
      onPressed: () {
        setState(() {

        });
        Navigator.of(context).pop();
      },
    );

    // set up the SimpleDialog
    SimpleDialog dialog = SimpleDialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))
      ),
      title: Column(
        children: <Widget>[
          Text('Ուշադրություն', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
          Text('Դուք պատրաստվում էք գնել',style: TextStyle(fontSize: 14.0)),
        ],
      ),
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            optionOne,
            optionTwo,
          ],
        )
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return dialog;
      },
    );
  }
}
