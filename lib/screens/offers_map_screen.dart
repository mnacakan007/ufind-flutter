import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ufind_flutter/blocs/offer/single_offer_bloc.dart';
import 'package:ufind_flutter/blocs/offers/offers_bloc.dart';
import 'package:ufind_flutter/models/offers_model.dart';
import 'package:ufind_flutter/screens/single_offer_screen.dart';

// ignore: must_be_immutable
class OffersMapScreen extends StatefulWidget {
  OfferLoaded state;

  OffersMapScreen(OfferLoaded state) {
    this.state = state;
  }

  @override
  _OffersMapScreenState createState() => _OffersMapScreenState();
}

class _OffersMapScreenState extends State<OffersMapScreen> {
  GoogleMapController _controller;

  List<Marker> allMarkers = [];
  List<Offer> offers = [];

  PageController _pageController;

  int prevPage;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (widget.state.offers.length != 0) {
      widget.state.offers.forEach((element) {
        allMarkers.add(Marker(
            markerId: MarkerId(element.partnerName),
            draggable: false,
            infoWindow: InfoWindow(
                title: element.partnerName, snippet: element.partnerState),
            position: LatLng(element.lat, element.lng)));
      });
      _pageController = PageController(initialPage: 1, viewportFraction: 0.8)
        ..addListener(_onScroll);
    }
  }

  void _onScroll() {
    if (_pageController.page.toInt() != prevPage) {
      prevPage = _pageController.page.toInt();
      moveCamera();
    }
  }

  _coffeeShopList(index, List<Offer> offers) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (BuildContext context, Widget widget) {
        double value = 1;
        if (_pageController.position.haveDimensions) {
          value = _pageController.page - index;
          value = (1 - (value.abs() * 0.3) + 0.06).clamp(0.0, 1.0);
        }
        return Center(
          child: SizedBox(
            height: Curves.easeInOut.transform(value) * 125.0,
            width: Curves.easeInOut.transform(value) * 350.0,
            child: widget,
          ),
        );
      },
      child: InkWell(
          onTap: () {
            context.bloc<SingleOfferBloc>().add(SingleOfferStarted(offerId: offers[index].id));
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      SingleOfferScreen(offers[index].title)),
            );
            // moveCamera();
          },
          child: Stack(children: [
            Center(
                child: Container(
                    margin: EdgeInsets.symmetric(
                      horizontal: 10.0,
                      vertical: 20.0,
                    ),
                    height: 125.0,
                    width: 275.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        boxShadow: [
                          BoxShadow(
                            color: Colors.black54,
                            offset: Offset(0.0, 4.0),
                            blurRadius: 10.0,
                          ),
                        ]),
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white),
                        child: Row(children: [
                          Container(
                              height: 90.0,
                              width: 90.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(10.0),
                                      topLeft: Radius.circular(10.0)),
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          offers[index].featuredImageUrl),
                                      fit: BoxFit.cover))),
                          SizedBox(width: 5.0),
                          Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  offers[index].partnerName,
                                  style: TextStyle(
                                      fontSize: 12.5,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  offers[index].partnerState,
                                  style: TextStyle(
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.w600),
                                ),
                                Container(
                                  width: 170.0,
                                  child: Text(
                                    offers[index].description.length > 65
                                        ? '${offers[index].description.substring(0, 65)}...'
                                        : offers[index].description,
                                    style: TextStyle(
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.w300),
                                  ),
                                )
                              ])
                        ]))))
          ])),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height - 50.0,
            width: MediaQuery.of(context).size.width,
            child: GoogleMap(
              initialCameraPosition: CameraPosition(
                  target: LatLng(40.177200, 44.503490), zoom: 12.0),
              markers: Set.from(allMarkers),
              onMapCreated: mapCreated,
            ),
          ),
          Positioned(
            bottom: 20.0,
            child: Container(
              height: 200.0,
              width: MediaQuery.of(context).size.width,
              child: widget.state.offers.length != 0 ? PageView.builder(
                controller: _pageController,
                itemCount: widget.state.offers.length,
                itemBuilder: (BuildContext context, int index) {
                  return _coffeeShopList(index, widget.state.offers);
                },
              ) : Container(),
            ),
          )
        ],
      ),
    );
  }

  void mapCreated(controller) {
    setState(() {
      _controller = controller;
    });
  }

  moveCamera() {
    _controller.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(
        target: LatLng(widget.state.offers[_pageController.page.toInt()].lat,
            widget.state.offers[_pageController.page.toInt()].lng),
        zoom: 14.0,
        bearing: 45.0,
        tilt: 45.0)));
  }
}
