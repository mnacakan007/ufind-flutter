import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/quiz/quiz_bloc.dart';
import 'package:ufind_flutter/blocs/survey/single_survey_bloc.dart';
import 'package:ufind_flutter/blocs/surveys/surveys_bloc.dart';
import 'package:ufind_flutter/models/single_survey_model.dart';
//import 'package:ufind_flutter/screens/quizzes_screen.dart';
//import 'package:ufind_flutter/screens/surveys_screen.dart';

// ignore: must_be_immutable
class SingleSurvey extends StatefulWidget {
  String title;
  String surveyId;
  String surveyName;

  SingleSurvey(String title, String surveyId, String surveyName) {
    this.title = title;
    this.surveyId = surveyId;
    this.surveyName = surveyName;
  }

  @override
  _SingleSurveyState createState() => _SingleSurveyState();
}

class _SingleSurveyState extends State<SingleSurvey> {
  List<dynamic> surveyOptions = [];
  var _onPressed;

  List<dynamic> checkbox = [
    {'value': false},
    {'value': false},
    {'value': false},
    {'value': false},
    {'value': false},
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: BlocBuilder<SingleSurveyBloc, SingleSurveyState>(
        builder: (context, state) {
          return Container(
            child: _buildBody(state),
          );
        },
      ),
    );
  }

  _buildBody(SingleSurveyState state) {
    if (state is SingleSurveyLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is SingleSurveyLoaded) {

      state.survey.options.forEach(
        (json) => surveyOptions.add(FieldOption.fromJson(json)),
      );

//      for(int i = 0; i < checkbox.length; i++) {
//        if(checkbox[i]['value'] == true) {
//          checkbox[i]['value'] = false;
//        }
//      }

      return state.survey.options.length != 0 && state.survey.status != 'finished'
          ? Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Text(state.survey.currentFieldName,
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold)),
                  ),
                  Expanded(
                    child: ListView.builder(
                      itemCount: state.survey.options.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
                          margin: EdgeInsets.all(10.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                constraints:
                                    BoxConstraints.tightFor(width: 100.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: <Widget>[
                                    Checkbox(
                                      value: checkbox[index]['value'],
                                      onChanged: (bool value) {
                                        setState(() {
                                          checkbox.forEach((v) => v['value'] = false);
                                          checkbox[index]['value'] = true;
                                          dynamic answer = {
                                            'values': [],
                                            'fieldId': '',
                                          };
                                            _onPressed = () {

                                              if (index != null) {

                                              setState(() { _onPressed = null; });

                                              answer['values'].add(surveyOptions[index].name);
                                              answer['fieldId'] = state.survey.currentFieldId;

                                              context.bloc<SingleSurveyBloc>().add(CreateSurveyAnswer(
                                                      surveyId: widget.surveyId,
                                                      answer: answer
                                              ));

                                              if(widget.surveyName == 'survey') {
                                                BlocProvider.of<SurveyBloc>(context).add(SurveyStarted());
                                              } else if (widget.surveyName == 'quiz') {
                                                BlocProvider.of<QuizBloc>(context).add(QuizStarted());
                                              }
                                            }
                                          };

                                        });
                                      },
                                    ),
                                    Container(
                                        width: 250.0,
                                        child: Text(surveyOptions[index].name)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: RaisedButton(
                      onPressed: _onPressed,
                      color: Colors.blue,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Container(
                        width: double.infinity,
                        height: 50.0,
                        child: Center(
                          child: Text(
                            'Հաստատել',
                            style:
                                TextStyle(color: Colors.white, fontSize: 16.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ])
          : Center(
              child: state.survey.status == 'finished' ?
              Column(
                children: <Widget>[
                  Expanded(
                    child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: Container(
                            padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
                            child: Text(
                              'Շնորհակալություն հարցմանը մասնակցելու համար։ Դուք վաստակել եք ${state.survey.pointsEarned} միավոր',
                              style: TextStyle(fontSize: 16.0),
                            )),
                    ),
                  ),),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.pop(context);

                        if(widget.surveyName == 'survey')
                          context.bloc<SurveyBloc>().add(SurveyStarted());
                        else
                          context.bloc<QuizBloc>().add(QuizStarted());
                      },
                      color: Colors.black87,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Container(
                        width: double.infinity,
                        height: 50.0,
                        child: Center(
                          child: Text(
                            'Փակել',
                            style:
                            TextStyle(color: Colors.white, fontSize: 16.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ) :
              Container(
                  padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
                  child: Text(
                    'Այս պահին նոր հարցումներ չունեք',
                    style: TextStyle(fontSize: 16.0),
                  )),
            );
    } else if (state is SingleSurveyError) {
      return Center(
        child: Text(
          'Error loading coins!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    } else if (state is SurveyAnswerLoading) {
        return Center(
          child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation(Theme.of(context).accentColor),
          ),
        );
      } else if (state is SurveyAnswerLoaded) {
        if(state.surveyAnswerResult != null && state.surveyAnswerResult.surveyId != null) {
          String surveyId = state.surveyAnswerResult.surveyId;
          context.bloc<SingleSurveyBloc>().add(SingleSurveyStarted(surveyId: surveyId));
        } else {
          context.bloc<SingleSurveyBloc>().add(SingleSurveyStarted(surveyId: widget.surveyId));
        }

    }
  }

}
