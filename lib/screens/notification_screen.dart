import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:ufind_flutter/blocs/notifications/notifications_bloc.dart';
import 'package:ufind_flutter/models/notification_model.dart';


class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {
  final _scrollController = ScrollController();
  var format = DateFormat.yMMMd().add_jms();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Ծանուցումներ', style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold)),
        ),
        body: BlocBuilder<NotificationBloc, NotificationState>(
          builder: (context, state) {
            return Container(
              child: _buildBody(state),
            );
          },
        ),
    );
  }

  _buildBody(NotificationState state) {
    if (state is NotificationLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is NotificationLoaded) {
      print('notifications ${state.notifications}');
      print('notifications ${state.notifications.length}');

      return state.notifications.length != 0
          ? Stack(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(20.0),
                child: Row(
                  children: <Widget>[
                    Text('Դուք ունեք ${state.notifications.length} ծանուցում',
                        style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: 60.0, left: 20.0, right: 20.0, bottom: 20.0),
                child: Container(
                  child: ListView.builder(
                    controller: _scrollController,
                    itemCount: state.notifications.length,
                    itemBuilder: (BuildContext context, int index) {
                      NotificationModel notification = state.notifications[index];
                      return Column(
                        children: <Widget>[
                          SizedBox(height: 10.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(notification.details),
                              Text(
                                  '${format.format(DateTime.fromMillisecondsSinceEpoch(notification.createdAt * 1000))}',
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      color: Colors.black38)),
                            ],
                          ),
                          SizedBox(height: 10.0),
                          Divider(
                            color: Colors.black,
                          ),
                        ],
                      );
                    },
                  ),
                ),
              )
            ],
          ) : Center(
        child: Container(
            padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
            child: Text(
              'Դուք հաղորդագրություններ չունեք',
              style: TextStyle(fontSize: 16.0),
            )),
      );
    } else if (state is NotificationError) {
      return Center(
        child: Text(
          'Error loading notifications!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }
}
