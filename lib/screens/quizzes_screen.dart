import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/points/points_bloc.dart';
import 'package:ufind_flutter/blocs/quiz/quiz_bloc.dart';
import 'package:ufind_flutter/blocs/survey/single_survey_bloc.dart';
import 'package:ufind_flutter/models/survey_model.dart';
import 'package:share/share.dart';
import 'package:ufind_flutter/screens/points/leaderBoardPageById.dart';
import 'package:ufind_flutter/screens/single_survey_screen.dart';

class Item {
  const Item(this.name, this.icon);

  final String name;
  final Icon icon;
}

class QuizzesScreen extends StatefulWidget {
  @override
  _QuizzesScreenState createState() => _QuizzesScreenState();
}

class _QuizzesScreenState extends State<QuizzesScreen> {
  final _scrollController = ScrollController();
  List<SurveysResult> surveyResults = [];
  List<SurveysResult> activeSurvey = [];
  String filterName = 'Բոլորը';
  dynamic quiz = [];
  Item selectedUser;

  List<Item> users = <Item>[
    const Item(
        'Բոլորը',
        Icon(
          Icons.done_all,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Չավարտված',
        Icon(
          Icons.done,
          color: const Color(0xFF167F67),
        )),
  ];

  @override
  void initState() {
    super.initState();
    context.bloc<QuizBloc>().add(QuizStarted());
    setState(() {
        selectedUser = users[1];
    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ցույց տալ'),
        actions: <Widget>[
          DropdownButton<Item>(
            hint: Text("Բոլորը"),
            value: selectedUser,
            onChanged: (Item value) {
              setState(() {
                selectedUser = value;
              });
            },
            items: users.map((Item user) {
              return DropdownMenuItem<Item>(
                value: user,
                child: Row(
                  children: <Widget>[
                    user.icon,
                    Text(
                      user.name,
                      style: TextStyle(color: Colors.black),
                    ),
                  ],
                ),
              );
            }).toList(),
          ),
//          Padding(
//            padding: const EdgeInsets.all(8.0),
//            child: RaisedButton(
//              color: Colors.white,
//              child: Text(filterName),
//              onPressed: () {
//                showAlertDialog(context);
//              },
//            ),
//          ),
        ],
      ),
      body: BlocBuilder<QuizBloc, QuizState>(
        builder: (context, state) {
          return Container(
            child: _buildBody(state),
          );
        },
      ),
    );
  }

  _buildBody(QuizState state) {
    if (state is QuizLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is QuizLoaded) {
      state.quizzes.forEach((json) => json.result != null
          ? surveyResults.add(SurveysResult.fromJson(json.result))
          : surveyResults.add(null));

      activeSurvey = surveyResults
          .where((i) => i != null && i.status == 'finished')
          .toList();

      return state.quizzes.length != 0
          ? RefreshIndicator(
        color: Theme.of(context).accentColor,
        onRefresh: () async {
//          context.bloc<QuizBloc>().add(RefreshQuiz());
        },
        child: NotificationListener<ScrollNotification>(
          onNotification: (notification) =>
              _onScrollNotification(notification, state),
          child: ListView.builder(
            controller: _scrollController,
            itemCount: state.quizzes.length,
            itemBuilder: (BuildContext context, int index) {
              quiz = state.quizzes[index];

              return Column(
                    children: <Widget>[
                      (surveyResults[index] == null || surveyResults[index] != null && surveyResults[index].status == 'in_progress') && selectedUser != null && selectedUser.name == 'Չավարտված' ?
                      quizCard(quiz, index) : Container(),
                      (selectedUser != null && selectedUser.name == 'Բոլորը') ?
                      quizCard(quiz, index) : Container(),
                    ],
                  );
            },
          ),
        ),
      ) : Center(
        child: Container(
            padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
            child: Text(
              'Այս պահին նոր հարցումներ չունեք',
              style: TextStyle(fontSize: 16.0),
            )),
      );
    } else if (state is QuizError) {
      return Center(
        child: Text(
          'Error loading quizzes!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  bool _onScrollNotification(ScrollNotification notif, QuizLoaded state) {
    if (notif is ScrollEndNotification &&
        _scrollController.position.extentAfter == 0) {
//      context.bloc<SurveyBloc>().add(LoadMoreSurveys(surveys: state.surveys));
    }
    return false;
  }

  surveyStatus(survey) {
    if (survey == null || survey.status == null) {
      return 'Սկսել'.toUpperCase();
    } else if (survey != null && survey.status == 'in_progress') {
      return 'Շարունակել'.toUpperCase();
    } else if (survey != null && survey.status == 'finished') {
      return 'Ավարտված'.toUpperCase();
    }
  }

  Widget quizCard(quiz, index) {
    String subject = 'Պատասխանիր և ստացիր U միավորներ';

    return Container(
      padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
      margin: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: double.infinity,
            constraints: BoxConstraints.tightFor(width: 100.0),
            child: Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(
                    quiz.featuredImageUrl,
                    fit: BoxFit.fitHeight,
                    width: double.infinity,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0, left: 15.0),
                      child: Container(
                        height: 30.0,
                        child: RaisedButton(
                          onPressed: quiz.title.isEmpty
                              ? null
                              : () {
                            final RenderBox box = context.findRenderObject();
                            Share.share(quiz.title,
                                subject: subject,
                                sharePositionOrigin:
                                box.localToGlobal(Offset.zero) &
                                box.size);
                          },
                          color: Colors.cyan,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.circular(5)),
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Կիսվել'.toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12.0),
                              ),
                              SizedBox(
                                width: 3.0,
                              ),
                              Icon(
                                Icons.share,
                                color: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 15.0, right: 15.0),
                      child: Container(
                        height: 30.0,
                        child: RaisedButton(
                          onPressed: () {
                            context.bloc<PointsBloc>().add(PointsStarted(surveyId: quiz.id));
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LeaderBoardByIdPage()),
                            );
                          },
                          color: Colors.cyan,
                          shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.circular(5)),
                          child: Row(
                            mainAxisAlignment:
                            MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'Առաջատարների ցանկ'.toUpperCase(),
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 12.0),
                              ),
                              SizedBox(width: 3.0),
                              Icon(
                                Icons.insert_chart,
                                color: Colors.white,
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          SizedBox(height: 15.0),
          Text(
            quiz.title,
            style: TextStyle(
                fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 15.0),
          Container(
              padding: EdgeInsets.fromLTRB(0, 3.0, 0, 3.0),
              child: Row(
                mainAxisAlignment:
                MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Row(children: <Widget>[
                      Icon(
                        Icons.account_balance_wallet,
                        color: Colors.amber,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        '${quiz.totalPointsMax} Միավոր',
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ]),
                  ),
                  Container(
                    height: 30.0,
                    child: RaisedButton(
                      onPressed: () {
                        if (surveyResults[index] != null &&
                            surveyResults[index].status ==
                                'finished') {
                          return null;
                        } else {
                          String surveyName = 'quiz';
                          context.bloc<SingleSurveyBloc>().add(
                              SingleSurveyStarted(
                                  surveyId: quiz.id));
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    SingleSurvey(quiz.title, quiz.id, surveyName)),
                          );
                        }
                      },
                      color: getSurveyStatusColor(
                          surveyResults[index]),
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.circular(5)),
                      child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            surveyStatus(surveyResults[index]),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12.0),
                          ),
                          SizedBox(
                            width: 3.0,
                          ),
                          surveyResults[index] != null &&
                              surveyResults[index].status ==
                                  'finished'
                              ? Container()
                              : Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the list options
    Widget optionOne = SimpleDialogOption(
      child: const Text('Բոլորը'),
      onPressed: () {
        setState(() {
          filterName = 'Բոլորը';
        });
        Navigator.of(context).pop();
      },
    );
    Widget optionTwo = SimpleDialogOption(
      child: const Text('Չավարտված'),
      onPressed: () {
        setState(() {
          filterName = 'Չավարտված';
        });
        Navigator.of(context).pop();
      },
    );

    // set up the SimpleDialog
    SimpleDialog dialog = SimpleDialog(
      title: const Text('Ցույց տալ'),
      children: <Widget>[
        optionOne,
        optionTwo,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return dialog;
      },
    );
  }

}

Color getSurveyStatusColor(survey) {
  if (survey != null && survey.status == 'finished') {
    return Colors.white10;
  } else {
    return Colors.blue;
  }
}
