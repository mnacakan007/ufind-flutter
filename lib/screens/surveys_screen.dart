import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ufind_flutter/blocs/survey/single_survey_bloc.dart';
import 'package:ufind_flutter/blocs/surveys/surveys_bloc.dart';
import 'package:ufind_flutter/screens/single_survey_screen.dart';
import 'package:ufind_flutter/models/survey_model.dart';
import 'package:share/share.dart';

class Item {
  const Item(this.name, this.icon);

  final String name;
  final Icon icon;
}

class SurveyScreen extends StatefulWidget {
  @override
  _SurveyScreenState createState() => _SurveyScreenState();
}

class _SurveyScreenState extends State<SurveyScreen> {
  final _scrollController = ScrollController();
  List<SurveysResult> surveyResults = [];
  List<SurveysResult> activeSurvey = [];
  String filterName = 'Բոլորը';
  dynamic survey = [];
  Item selectedUser;

  List<Item> users = <Item>[
    const Item(
        'Բոլորը',
        Icon(
          Icons.done_all,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Չավարտված',
        Icon(
          Icons.done,
          color: const Color(0xFF167F67),
        )),
  ];

  @override
  void initState() {
    super.initState();
    context.bloc<SurveyBloc>().add(SurveyStarted());
    setState(() {
        selectedUser = users[1];
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Ցույց տալ'),
          actions: <Widget>[
            DropdownButton<Item>(
              hint: Text("Բոլորը"),
              value: selectedUser,
              onChanged: (Item value) {
                setState(() {
                  selectedUser = value;
                });
              },
              items: users.map((Item user) {
                return DropdownMenuItem<Item>(
                  value: user,
                  child: Row(
                    children: <Widget>[
                      user.icon,
                      Text(
                        user.name,
                        style: TextStyle(color: Colors.black),
                      ),
                    ],
                  ),
                );
              }).toList(),
            ),
//          Padding(
//            padding: const EdgeInsets.all(8.0),
//            child: RaisedButton(
//              color: Colors.white,
//              child: Text(filterName),
//              onPressed: () {
//                showAlertDialog(context);
//              },
//            ),
//          ),
          ],
        ),
        body: BlocBuilder<SurveyBloc, SurveyState>(
          builder: (context, state) {
            return Container(
              child: _buildBody(state),
            );
          },
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child: Text("NO"),
          ),
          SizedBox(height: 16),
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(true),
            child: Text("YES"),
          ),
        ],
      ),
    ) ??
        false;
  }

  _buildBody(SurveyState state) {
    if (state is SurveyLoading) {
      return Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation(Theme.of(context).colorScheme.primary),
        ),
      );
    } else if (state is SurveyLoaded) {
      state.surveys.forEach((json) => json.result != null
          ? surveyResults.add(SurveysResult.fromJson(json.result))
          : surveyResults.add(null));

      activeSurvey = surveyResults
          .where((i) => i != null && i.status == 'finished')
          .toList();

      return state.surveys.length != 0
          ? RefreshIndicator(
        color: Theme.of(context).accentColor,
        onRefresh: () async {
//          context.bloc<SurveyBloc>().add(RefreshSurveys());
        },
        child: NotificationListener<ScrollNotification>(
          onNotification: (notification) =>
              _onScrollNotification(notification, state),
          child: ListView.builder(
            controller: _scrollController,
            itemCount: state.surveys.length,
            itemBuilder: (BuildContext context, int index) {
              survey = state.surveys[index];

              return Column(
                    children: <Widget>[
                      (surveyResults[index] == null || surveyResults[index] != null && surveyResults[index].status == 'in_progress') && selectedUser != null && selectedUser.name == 'Չավարտված' ?
                      surveyCard(survey, index) : Container(),
                      (selectedUser != null && selectedUser.name == 'Բոլորը') ?
                      surveyCard(survey, index) : Container(),
                    ],
                  );
            },
          ),
        ),
      ) : Center(
        child: Container(
            padding: EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
            child: Text(
              'Այս պահին նոր հարցումներ չունեք',
              style: TextStyle(fontSize: 16.0),
            )),
      );
    } else if (state is SurveyError) {
      return Center(
        child: Text(
          'Error loading surveys!\nPlease check your connection',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontSize: 18.0,
          ),
          textAlign: TextAlign.center,
        ),
      );
    }
  }

  bool _onScrollNotification(ScrollNotification notif, SurveyLoaded state) {
    if (notif is ScrollEndNotification &&
        _scrollController.position.extentAfter == 0) {
//      context.bloc<SurveyBloc>().add(LoadMoreSurveys(surveys: state.surveys));
    }
    return false;
  }

  surveyStatus(survey) {
    if (survey == null || survey.status == null) {
      return 'Սկսել'.toUpperCase();
    } else if (survey != null && survey.status == 'in_progress') {
      return 'Շարունակել'.toUpperCase();
    } else if (survey != null && survey.status == 'finished') {
      return 'Ավարտված'.toUpperCase();
    }
  }

  Widget surveyCard(survey, index) {
    String subject = 'Պատասխանիր և ստացիր U միավորներ';

    return Container(
      padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
      margin: EdgeInsets.all(10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            width: double.infinity,
            constraints: BoxConstraints.tightFor(width: 100.0),
            child: Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Image.network(
                    survey.featuredImageUrl,
                    fit: BoxFit.fitHeight,
                    width: double.infinity,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      top: 15.0, left: 15.0),
                  child: Container(
                    width: 110.0,
                    height: 30.0,
                    child: RaisedButton(
                      onPressed: survey.title.isEmpty
                          ? null
                          : () {
                        final RenderBox box = context.findRenderObject();
                        Share.share(survey.title,
                            subject: subject,
                            sharePositionOrigin:
                            box.localToGlobal(Offset.zero) &
                            box.size);
                      },
                      color: Colors.cyan,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.circular(5)),
                      child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            'Կիսվել'.toUpperCase(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12.0),
                          ),
                          SizedBox(
                            width: 3.0,
                          ),
                          Icon(
                            Icons.share,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 15.0),
          Text(
            survey.title,
            style: TextStyle(
                fontSize: 16.0, fontWeight: FontWeight.bold),
          ),
          SizedBox(height: 15.0),
          survey.estimation > 0
              ? Container(
              padding: EdgeInsets.fromLTRB(0, 3.0, 0, 3.0),
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.access_alarms,
                    color: Colors.amber,
                  ),
                  SizedBox(
                    width: 5.0,
                  ),
                  Text(
                    '${survey.estimation}ր',
                    style: TextStyle(fontSize: 16.0),
                  ),
                ],
              ))
              : Container(),
          Container(
              padding: EdgeInsets.fromLTRB(0, 3.0, 0, 3.0),
              child: Row(
                mainAxisAlignment:
                MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Row(children: <Widget>[
                      Icon(
                        Icons.account_balance_wallet,
                        color: Colors.amber,
                      ),
                      SizedBox(
                        width: 5.0,
                      ),
                      Text(
                        '${survey.totalPointsMax} Միավոր',
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ]),
                  ),
                  Container(
                    height: 30.0,
                    child: RaisedButton(
                      onPressed: () {
                        if (surveyResults[index] != null &&
                            surveyResults[index].status ==
                                'finished') {
                          return null;
                        } else {
                          String surveyName = 'survey';
                          context.bloc<SingleSurveyBloc>().add(SingleSurveyStarted(surveyId: survey.id));
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    SingleSurvey(survey.title, survey.id, surveyName)),
                          );
                        }
                      },
                      color: getSurveyStatusColor(
                          surveyResults[index]),
                      shape: RoundedRectangleBorder(
                          borderRadius:
                          BorderRadius.circular(5)),
                      child: Row(
                        mainAxisAlignment:
                        MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            surveyStatus(surveyResults[index]),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 12.0),
                          ),
                          SizedBox(
                            width: 3.0,
                          ),
                          surveyResults[index] != null &&
                              surveyResults[index].status ==
                                  'finished'
                              ? Container()
                              : Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // set up the list options
    Widget optionOne = SimpleDialogOption(
      child: const Text('Բոլորը'),
      onPressed: () {
        setState(() {
          filterName = 'Բոլորը';
        });
        Navigator.of(context).pop();
      },
    );
    Widget optionTwo = SimpleDialogOption(
      child: const Text('Չավարտված'),
      onPressed: () {
        setState(() {
          filterName = 'Չավարտված';
        });
        Navigator.of(context).pop();
      },
    );

    // set up the SimpleDialog
    SimpleDialog dialog = SimpleDialog(
      title: const Text('Ցույց տալ'),
      children: <Widget>[
        optionOne,
        optionTwo,
      ],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return dialog;
      },
    );
  }

}

Color getSurveyStatusColor(survey) {
  if (survey != null && survey.status == 'finished') {
    return Colors.white10;
  } else {
    return Colors.blue;
  }
}
